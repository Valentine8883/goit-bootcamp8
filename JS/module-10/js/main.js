'use strict';
/*
  Написать приложение для работы с REST сервисом, 
  все функции делают запрос и возвращают Promise 
  с которым потом можно работать. 
  
  Реализовать следующий функционал:
  - функция getAllUsers() - должна вернуть текущий список всех пользователей в БД.
  
  - функция getUserById(id) - должна вернуть пользователя с переданным id.
  
  - функция addUser(name, age) - должна записывать в БД юзера с полями name и age.
  
  - функция removeUser(id) - должна удалять из БД юзера по указанному id.
  
  - функция updateUser(id, user) - должна обновлять данные пользователя по id. 
    user это объект с новыми полями name и age.
  Документацию по бэкенду и пример использования прочитайте 
  в документации https://github.com/trostinsky/users-api#users-api.
  Сделать минимальный графический интерфейс в виде панели с полями и кнопками. 
  А так же панелью для вывода результатов операций с бэкендом.
*/
const url = 'https://test-users-api.herokuapp.com/users/';
const getAllUsersBtn = document.querySelector('.js-get-all-users');
const getUserByIdBtn = document.querySelector('.js-users-by-id');
const addUserBtn = document.querySelector('.js-add-user');
const removeUserBtn = document.querySelector('.js-remove-user');
const updateUserBtn = document.querySelector('.js-update-user');
const inputUserId = document.querySelector('.input-user-id');
const inputUserIdRemove = document.querySelector('.input-user-id-remove');
const inputUserIdUpdate = document.querySelector('.input-user-id-update')
const list = document.querySelector('.js-list');

getAllUsersBtn.addEventListener('click', getAllUsers);
getUserByIdBtn.addEventListener('click', () => getUserById(inputUserId.value));
addUserBtn.addEventListener('click', getData.bind(null, addUser));
removeUserBtn.addEventListener('click', () => removeUser(inputUserIdRemove.value));
updateUserBtn.addEventListener('click', getDatatoUpdate.bind(null, updateUser));

function getAllUsers() {
    event.preventDefault()
    fetch(url)
    .then(request => {
        if(request.ok) return request.json();
        throw new Error(`GET function error# ${request.status}`);
    })
    .then(data => {
        list.innerHTML = '';
        data.data.forEach(({id, name, age}) => 
        list.insertAdjacentHTML('beforeend', `<li class='js-list-item'>ID: ${id}<br>NAME: ${name}<br>AGE: ${age}<li>`))
    })
    .catch(error => list.insertAdjacentHTML('beforeend', `<li class='error'>${error}<li>`));
}

function getUserById(id) {
    event.preventDefault();
    list.innerHTML = '';
    if(id) {
        fetch(url+id)
        .then(request => {
            if(request.ok) return request.json();
            throw new Error(`GET by id function error# ${request.status}`)
        })
        .then(x =>  {
            if(x.status === 500 || x.status === 404) {
                list.insertAdjacentHTML('beforeend',`<li class="error">user not found<li>`);
            } else {
                list.insertAdjacentHTML('beforeend', `<li class='js-list-item'>ID: ${x.data.id}<br>NAME: ${x.data.name}<br>AGE: ${x.data.age}<li>`);
            }
        }).catch(error => list.insertAdjacentHTML('beforeend',`<li class="error">${error}<li>`))
    } else {
        list.insertAdjacentHTML('beforeend',`<li class="error">Please, enter some id<li>`);
    }
}

function addUser(name, age) {
    event.preventDefault();
    fetch('https://test-users-api.herokuapp.com/users', {
        method: 'POST',
        body: JSON.stringify({'name': name, 'age': age}),
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
    }
})
    .then(request => {
        if(request.ok) return request.json();
        throw new Error(`POST function error# ${request.status}`)
    })
    .then(x => {
        list.insertAdjacentHTML('afterbegin', `<li class='succsess'>New user added<br>ID: ${x.data._id}<br>NAME: ${x.data.name}<br>AGE: ${x.data.age}<li>`);
    })
    .catch(error => list.insertAdjacentHTML('beforeend',`<li class="error">${error}<li>`));
}

function removeUser(id) {
    event.preventDefault();
    list.innerHTML = '';
    if(id) {
        fetch(url + id, {
            method: 'DELETE',
          })
        .then(request => {
            if(request.ok) return request.json();
            throw new Error(`DELETE function error# ${request.status}`) ;
        })
        .then(data => {
            if(data.hasOwnProperty('data') && data.data !== null && data.data.id.toLowerCase() === id.toLowerCase()) {
                let {id, name, age} = data.data;
                list.insertAdjacentHTML('beforeend', `<li class='succsess'>User remove<br>ID: ${id}<br>NAME: ${name}<br>AGE: ${age}<li>`);
            } else {
                list.insertAdjacentHTML('beforeend',`<li class="error">id not found<li>`);
            }
        })
        .catch(error => list.insertAdjacentHTML('beforeend',`<li class="error">${error}<li>`));
    } else {
        list.insertAdjacentHTML('beforeend',`<li class="error">Please, enter some id<li>`);
    }
}

function updateUser(id, user) {
    fetch(url+id, {
        method: 'PUT',
        body: JSON.stringify(user),
        headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        }
    })
    .then(request => {
        if(request.ok) return request.json();
        throw new Error(`UPDATE function error# ${request.status}`) ;
    })
    .then(data => {
        if(data.hasOwnProperty('data') && data.data !== null && data.data.id.toLowerCase() === id.toLowerCase()) {
            let {id, name, age} = data.data;
            list.insertAdjacentHTML('beforeend', `<li class='succsess'>User update<br>ID: ${id}<br>NAME: ${name}<br>AGE: ${age}<li>`);
        } else {
            list.insertAdjacentHTML('beforeend',`<li class="error">id not found<li>`);
        }
    })
}

function getData(callback) {
    while(true) {
        let name = prompt('Please, enter a name');
        if(name === '' || name === null) {
            alert('string with name cannot be empty, try again')
            break;
        }
        let age = Number.parseInt(prompt('Please, enter an age'));
        if(age % 1 !== 0 || age === null) {
            console.log(age);
            alert('age must be a number, try again')
            break;
        }
        if(name.length > 0 && age % 1 === 0) {
            callback(name, age);
            break;
        }
    }
}

function getDatatoUpdate(callback) {
    event.preventDefault();
    list.innerHTML = '';
    if(inputUserIdUpdate.value === "") {
        list.insertAdjacentHTML('beforeend',`<li class="error">Please, enter some id<li>`);
        return;
    }
    while(true) {
        let name = prompt('Please, enter a name');
        if(name === '' || name === null) {
            alert('string with name cannot be empty, try again')
            break;
        }
        let age = Number.parseInt(prompt('Please, enter an age'));
        if(age % 1 !== 0 || age === null) {
            console.log(age);
            alert('age must be a number, try again')
            break;
        }
        if(name.length > 0 && age % 1 === 0) {
            callback(inputUserIdUpdate.value, {name, age});
            break;
        }
    }
}