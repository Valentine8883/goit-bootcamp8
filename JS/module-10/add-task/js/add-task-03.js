'use strict';
/*
  Документация API: https://github.com/trostinsky/users-api#users-api

  Просмотр всех записей: https://test-users-api.herokuapp.com/users/ 

  Написать функцию fetchUsers, которая используя REST сервис 
  по адресу https://test-users-api.herokuapp.com/users/
  посылает get запрос и получает ответ.
  
  Результатом fetch будет массив объектов с полями.
  
  В элемент result поместить таблицу состоящую из 2-х
  столбцов след формата, где кол-во строк будет такое как
  и кол-во объектов пользователей в ответе:
  
    ID | NAME | AGE
    id | name | age  
    id | name | age  
*/

const getBtn = document.querySelector(".js-get");
const result = document.querySelector(".result");
const url = 'https://test-users-api.herokuapp.com/users/';

getBtn.addEventListener("click", fetchUsers);

/*
  @param {FormEvent} evt
*/
function fetchUsers(evt) {
  evt.preventDefault();
  fetch(url)
  .then(request => request.json())
  .then(data => getValue(data.data))
  .catch(error => result.innerHTML = error)
  
}

function getValue(data) {
    result.innerHTML = '';
    data.forEach(({id, name, age}) => result.insertAdjacentHTML('beforeend', `<p>${id} | ${name} | ${age}</p>`));
}