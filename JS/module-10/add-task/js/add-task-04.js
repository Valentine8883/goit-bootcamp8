'use strict';

/*
  Документация API: https://github.com/trostinsky/users-api#users-api

  Просмотр всех записей: https://test-users-api.herokuapp.com/users/ 

  Написать функцию getUserByName, которая используя REST сервис 
  по адресу https://test-users-api.herokuapp.com/users/
  посылает запрос с name введенным в input.
 
  Результатом fetch будет ответ от сервера, 
  вывести содержимое всего ответа (id, name, age) 
  или 'Такого пользователя в списке нет!'.
*/

const input = document.querySelector("input");
const postBtn = document.querySelector(".js-post");
const result = document.querySelector(".result");
const url = 'https://test-users-api.herokuapp.com/users/';

postBtn.addEventListener("click", getUserByName);

function getUserByName(evt) {
  evt.preventDefault();
  fetch(url)
  .then(request => request.json())
  .then(data =>  {
      result.innerHTML = '';
      const filterName = data.data.filter((x => x.name === input.value));
      console.log(filterName);
      filterName.length ? filterName.forEach(({id, name, age}) => result.insertAdjacentHTML("beforeend", `<p>${id} | ${name} | ${age}</p>`)) : result.textContent = 'Такого пользователя в списке нет!'

    })
}

