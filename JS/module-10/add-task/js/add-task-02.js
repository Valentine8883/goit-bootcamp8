'use strict';


/*
  Написать функцию fetchUserData, которая использует
  apiUrl + текущее значение input для составления запроса.
  
  Формат полного url таков:
    https://api.github.com/users/имя-пользователя
    
  Документация по Git API:
    https://developer.github.com/v3/
    
  С помощью fetch сделать запрос по составленому адресу. 
  Обязательно обработать вариант с ошибкой запроса используя catch. 
  
  Результат запроса вывести в поле result в формате:
    Avatar: аватартка 
    Username: логин
    Bio: описание профиля
    Public repos: кол-во открытых репозиториев
  
  Все необходимые данные есть в ответе от API.
*/

const input = document.querySelector("input");
const submitBtn = document.querySelector("#js-submit");
const result = document.querySelector(".result");
const apiUrl = "https://api.github.com/users/";

submitBtn.addEventListener("click", fetchUserData);

/*
  @param {FormEvent} evt
*/
function fetchUserData(event) {
    event.preventDefault();

    fetch(apiUrl+input.value)
    .then(request => {

      if(request.ok) return request.json();
      
      throw new Error('Error#' + request.status)
    })
    .then(({avatar_url, login, public_repos, bio}) => {
      createUI(avatar_url, login, public_repos, bio); 
    })
    .catch(error => result.textContent = error)

}

function createUI(avatarUrl, login, repos, bio) {
  result.innerHTML = '';
  let avatar = document.createElement('img');
  avatar.src = avatarUrl;
  avatar.height = 100;
  let loginP = document.createElement('p');
  loginP.textContent = 'Username: ' + login;
  let reposP = document.createElement('p');
  reposP.textContent = 'Public repos: ' + repos;
  let bioP = document.createElement('p');
  bioP.textContent = bio !== null ? 'Bio: '+ bio :'Bio: no data';
  result.append(avatar, loginP, reposP, bioP);
}





















































// console.log(localStorage);

// localStorage.setItem('key', JSON.stringify([5]));
// let result = localStorage.getItem('key');
// console.log(JSON.parse(result));


// localStorage.removeItem('key');

// const fontSize = document.querySelector('#fontSize');
// const fontFamily = document.querySelector('#fontFamily');
// const color = document.querySelector('#color');
// const form = document.querySelector('#form');

// form.addEventListener('click', readValue);

// function readValue(e) {
//     e.preventDefault()
//     localStorage.setItem('settings', JSON.stringify({
//         fontFamily: fontFamily.value,
//         fontSize: fontSize.value, 
//         fontColor: color.value,
       
//     }));
//     getStaorageData();
// }

// function getStaorageData() {
//     let result = JSON.parse(localStorage.getItem('settings'));
//     let allP = [...document.querySelectorAll('p')];
//     for(let el of allP) {
//         el.style.fontSize = result.fontSize + 'px';
//         el.style.fontFamily = result.fontFamily;
//         el.style.color = result.fontColor;
//     }
// }

// addEventListener('DOMContentLoaded', getStaorageData());