'use strict';
/*
  Написать функцию fetchCountryData, которая использует
  apiUrl + текущее значение input для составления запроса.
  
  Формат полного url таков:
    https://restcountries.eu/rest/v2/name/имя-страны
    
  С помощью fetch сделать запрос по составленому 
  адресу. Обязательно обработать вариант с ошибкой запроса
  используя catch. 
  
  Результат запроса вывести в поле result в формате:
    Country name: имя страны
    Capital: столица
    Main currency: название денежной единицы
    Flag: флаг страны
  
  Все необходимые данные есть в ответе от API.
  
  PS: при отправке формы перезагружается страница,
  решите эту задачу вспомнив о том, как остановить
  поведение по умолчанию.
*/

const input = document.querySelector("input");
const submitBtn = document.querySelector(".js-submit");
const result = document.querySelector(".js-result");
const apiUrl = `https://restcountries.eu/rest/v2/name/`;
submitBtn.addEventListener("click", fetchCountryData);

/*
  @param {FormEvent} evt
*/
function fetchCountryData(event) {

    event.preventDefault()
    fetch(apiUrl+input.value)
    .then(response => {
        if(response.ok) return response.json();
        throw new Error(`#${response.status}`);
    })
    .then(data =>  {
        const {name, capital, currencies, flag} = data[0];
        result.innerHTML = `Country name: ${name}<br>
        Capital: ${capital}<br>Main currency: ${currencies[0].name}
        <img src="${flag}" style="width:500px; display: block;">`
    })
    .catch(error => result.textContent = error);
}

/*
const url = `https://jsonplaceholder.typicode.com/posts/`; // путь к ресурсу. Доступные пути описываются в документации REST-сервиса
// Доступные пути описываются при создании REST-сервиса в его документации.

const newPost = {
    author: 'Mango', 
    body: 'CRUD 8883',
};

const options = {
    method: 'POST', //HTTP-метод — определяет какую операцию выполнять
    headers: { // Заголовок — позволяет клиенту передавать информацию о запросе
        'Accept': 'application/json', //заголовок Accept  - тип контента который клиент может обработать (ожтдает)
        'Content-Type': 'application/json', // заголовок Content-Type - описывает тип ресурса который клиент ожидает в ответе от REST-сервиса

    },
    body: JSON.stringify(newPost), // Тело — дополнительный блок запроса, содержащий данные
};

fetch(url, options).then(result => {
    if(result.ok) return result.json();
    throw new Error('ERROR 8883');
})
.then(post => console.log(post))
.catch(error => console.log(error));


///////////////////////////////////////////////////
function getPostByID(id) {
    return fetch(url+id)
    .then(response => response.json());
}

getPostByID(8).then(post => console.log(post))

///////////////////////////////////////////////////

const postToUpdate = {
    body: 'new CRUD 8883',
};

fetch(url+8, {
    method: 'PUT',
    body: JSON.stringify(postToUpdate),
    headers: {
        'Content-type': 'application/json; charset=UTF-8',
    },
})
.then(result => result.json())
.then(data => console.log(data))

///////////////////////////////////////////////////


function deletePostByID(id) {
    fetch(url+id, {
        method: 'DELETE',
    })
    .then(response => response.ok ? console.log('succsess') : console.log('not succsess'))
    .catch(error => console.log('ERRRRRRROR' + error))
}

deletePostByID(9999999);

// POST тело -> Ответ с добавленным id -> обновили интерфейс
// GET -> ответ с данными -> обновили интерфейс
// PUT тело -> ответ с обновленными данными -> обновили интерфейс
// DELETE -> ответ со статусом 200 -> обновили интерфейс
*/