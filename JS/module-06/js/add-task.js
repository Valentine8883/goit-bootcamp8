/* task-01 finished
  Создать функцию-конструктор Account, которая добавляет будущему
  объекту поля login, email и friendsCount. 
  
  В prototype функции-конструктора добавить метод getAccountInfo(), 
  который выводит в консоль значения полей login, email и friendsCount. 
  
  Обратите внимание, метод будет всего один, в поле prototype функции-конструктора, 
  а использовать его смогут все экземпляры, по ссылке.
  
  Создать несколько экземпляров с разными значениями свойств, вывести их в консоль.
*/

// function Account(login, email, friendsCount) {
//     this.login = login;
//     this.email = email;
//     this.friendsCount = friendsCount;
// }

// Account.prototype.getAccountInfo = function() {
//   console.log(`login: ${this.login}, email: ${this.email}, frieds count: ${this.friendsCount}`);
// }

// const a1 = new Account('Admin', 'admin@me.com', 5934);
// const a2 = new Account('User', 'user@me.com', 345);

// a1.getAccountInfo();
// a2.getAccountInfo();

// console.log(a1);
// console.log(a2);

/***************************************************************************************************/
/* task-02 finished
  Напишите функцию-конструктор StringBuilder.
  
  На вход она получает один параметр string - строку.
  
  Добавьте следующие методы в prototype функции-конструктора.
  
    - getValue() - выводит в консоль текущее значение поля value
  
    - append(str) - получает парметр str - строку и добавляет 
      ее в конец значения поля value
    
    - prepend(str) - получает парметр str - строку и добавляет 
      ее в начало значения поля value
  
    - pad(str) - получает парметр str - строку и добавляет 
      ее в начало и в конец значения поля value
*/
/*
function StringBuilder(string = "") {
    this.value = string;
  }

StringBuilder.prototype.showValue = function () {
  console.log(this.value);
}

StringBuilder.prototype.append = function(str) {
  this.value += str;
}
  
StringBuilder.prototype.prepend = function(str) {
  this.value = str + this.value;
}
StringBuilder.prototype.pad = function(str) {
  this.value = `${str}${this.value}${str}`;
}

  const myString = new StringBuilder('.');
  
  myString.append('^'); 
  myString.showValue(); // '.^'
  
  myString.prepend('^'); 
  myString.showValue(); // '^.^'
  
  myString.pad('='); 
  myString.showValue(); // '=^.^='
*/
/***************************************************************************************************/
/* task-03 finished
  Создайте класс Car с указанными полями и методами.
*/
class Car {
  constructor(maxSpeed, value) {
    /*
  Добавьте классу Car свойство value - цена автомобиля.
  
  Добавьте классу Car использование геттеров и сеттеров для свойства value.
  
  Геттер вернет текущей значение поля this._value
  Сеттер запишет в поле this._value то что ему присвоят
  
  PS: имя геттера и сеттера не может совпадать с полем, поэтому используйте
    не this.value а this._value
    
  Использование выглядит следующим образом:
  
  const myCar = new Car(50, 2000);
  
  myCar.value; // 2000
  myCar.value = 4000;
  myCar.value; // 4000

*/
/* task-05
  Добавьте свойства:
    - speed - для отслеживания текущей скорости, изначально 0.
    
    - maxSpeed - для хранения максимальной скорости 
    
    - running - для отслеживания заведен ли автомобиль, 
      возможные значения true или false. Изначально false.
      
    - distance - содержит общий киллометраж, изначально с 0
*/
   this._value = value;
   this.speed = 0;
   this.maxSpeed = maxSpeed;
   this.running = false;
   this.distanse = 0;
  }

  /* task-04 finished
  Добавьте к классу Car из предыдущего задания статический
  метод getSpecs, который получает объект-машину как аргумент
  и выводит в консоль значения полей maxSpeed, running и distance.
  
  Использование будет выглядеть следующим образом:
  
  const someCar = new Car(100);
  someCar.turnOn();
  someCar.drive(2);
  
  Car.getSpecs(someCar); // maxSpeed: 100, running: true, distance: 200
*/
  static getSpecs(car) {
    console.log(`Max speed: ${car.maxSpeed}\nRunning: ${car.running}\nDistance: ${car.distanse}`);
  }

  get value() {
    return this._value;
  }

  set value(value) {
    this._value = value;
  }

  turnOn() {
    // Добавьте код для того чтобы завести автомобиль
    // Просто записывает в свойство running значание true
    this.running = true;
  }

  turnOff() {
    // Добавьте код для того чтобы заглушить автомобиль
    // Просто записывает в свойство running значание false
    this.running = false;
  }
  
  accelerate(spd) {
    // Записывает в поле speed полученное значение, при условии что
    // оно не больше чем значение свойства maxSpeed
    spd <= this.maxSpeed ? this.speed = spd : -1;
  }
  
  decelerate(spd) {
    // Записывает в поле speed полученное значение, при условии что
    // оно не больше чем значение свойства maxSpeed и не меньше нуля
    spd  <= this.maxSpeed && spd > 0 ? this.speed = spd : -1;
  }

  drive(hours) {
    // Добавляет в поле distance киллометраж (hours умноженное на значение поля speed),
    // но только в том случае если машина заведена!
    this.distanse = hours * this.speed;
  }
}

const myCar = new Car(50, 2000);
myCar.turnOn();
myCar.accelerate(150);
myCar.drive(4);
Car.getSpecs(myCar);

myCar.value; // 2000
myCar.value = 4000;
myCar.value; // 4000

console.log(myCar);

/***************************************************************************************************/