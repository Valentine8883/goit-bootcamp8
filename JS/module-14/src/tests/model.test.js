import Model from '../js/model';

describe('Model class', () => {
    it('Should create Model instance', () => {
        const model = new Model();
        expect(model instanceof Model).toBe(true);
    });

    it('Should contain starting items', () => {
        const model = new Model();
        expect(model.items).toEqual([]);
    });

    // it('Should add item to items', () => {
    //     const model = new Model();
    //     model.addItem('http://some-url.com');
    //     expect(model.items).toContain({url:'http://some-url.com'});
    // });

    it('Should remove item from items', () => {
        const model = new Model();
        model.removeItem('12345');
        expect(model.items).not.toContain({id:'12345'})
    });


    // it('Should return null if item does not')



});