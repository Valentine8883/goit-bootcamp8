export default class Controller{
    constructor(model, view) {
        this.model = model;
        this.view = view;
        this.view.on('add', this.addSite.bind(this));
        this.view.on('remove', this.removeSite.bind(this));
        window.addEventListener('DOMContentLoaded', () => this.model.items.length? this.getStorageData(this.model.items) : null)
    }
    addSite(url) {
        this.model.addItem(url).then(item => this.view.addSite(item));
    }

    removeSite(id) {
        this.model.removeItem(id);
        this.view.removeSite(id); 
    }

    getStorageData(items) {
        items.forEach(x=> this.view.addSite(x))
    }
}