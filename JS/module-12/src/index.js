import './css/styles.css';
import './scss/styles.scss';
import template from './templates/siteItem.hbs';
import {getSiteImg} from './services/api';
import * as storage from './services/storage';

const siteInput = document.querySelector('.url-input');
const addBtn = document.querySelector('.js-add-site');
const siteList = document.querySelector('.js-list');
const urlPattern = /^((https||http)?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/i;
const storageCheck = storage.get();
const fetchSites = storageCheck ? storageCheck : [];
addEventListener('DOMContentLoaded', () => siteList.insertAdjacentHTML('afterbegin',template({sites: fetchSites})));

function inputValue() {
  return siteInput.value.toLowerCase();
}

addBtn.addEventListener('click', () => {
  event.preventDefault();
  if(urlPattern.test(inputValue())) {
    if(uniqueLink(inputValue())) {
        getSiteImg(inputValue()).then((data) => {
        fetchSites.unshift(createItem(data));
        storage.set(fetchSites);
      });
    } else {
      alert('URL is not unique');
    }
  } else {
    alert("URL is not valid, try again");
  }
});

function createItem({image, description}) {
    let sites = [{img: '', url:inputValue(), description:''}];
    image ? sites[0].img = image : sites[0].img = 'https://blogbringit.com.br/wp-content/uploads/2016/11/banner_404-1170x500.jpg';
    sites[0].description = description;
    siteList.insertAdjacentHTML('afterbegin',template({sites: sites}));
    return sites[0];
}

function uniqueLink() {
  let unique = true;
  document.querySelectorAll('.js-site-item').forEach(x => x.firstChild.textContent.trim().toLowerCase().includes(inputValue()) ? unique = false : null );
  return unique;
}

siteList.addEventListener('click', removeItem);
function removeItem({target}) {
  event.preventDefault();
  if(target.className.includes('js-remove-item-btn')) {
    target.parentNode.remove();
    fetchSites.forEach((x, idx) => x.url.toLowerCase() === target.parentNode.firstChild.textContent.trim().toLowerCase() ? fetchSites.splice(idx, 1) : null);
    storage.remove;
    storage.set(fetchSites);
  }   
}