'use strict'

/* task-01 finished
  Есть массив имен пользователей.
  В первом console.log вывести длину массива.
  
  В последующих console.log дополнить конструкцию
  так, чтобы в консоль вывелись указаные в комментариях 
  элементы массива.
*/

// const users = ['Mango', 'Poly', 'Ajax', 'Chelsey'];

// console.log(users.length); // 4

// console.log(users[1]); // Poly
// console.log(users[3]); // Chelsey
// console.log(users[0]); // Mango
// console.log(users[2]); // Ajax

/* ************************************************ */

/*  task-02 finished
Есть массив имен пользователей 
*/

// const users = ["Mango", "Poly", "Ajax", "Chelsey"];

// /* Используя методы массива, последовательно выполнить следующие операции */

// // Удалить из начала массива нулевой элемент
// users.shift();
// console.log(users); // ["Poly", "Ajax", "Chelsey"]

// // Удалить из конца массив последний элемент
// users.pop();
// console.log(users); // ["Poly", "Ajax"]

// // Добавить в начало массива любое имя
// users.unshift('myName');
// console.log(users); // ["добавленое имя", "Poly", "Ajax"]

// // Добавить в конец массива два любых имени за одну операцию
// users.push('name1', 'name2');
// console.log(users); //  ["добавленое ранее имя", "Poly", "Ajax", "имя 1", "имя 2"]

/* ************************************************ */

/* task-03 finished
  Попросить пользователя ввести произвольную строку
  и записать ее в переменную string
  
  PS: для перебора массива используте цикл for или for...of
*/

// let string = prompt('Please, enter an arbitrary string');

// let arr = string.split('');


// // // Вывести в консоли общую длину массива arr
// console.log('length: ' + arr.length);

// // // Используя цикл, вывести в консоль все индексы массива arr
// // console.log();

// for(let i = 0; i < arr.length; i+=1) {
//   console.log('idx: ' + i);
// }

// // // Используя цикл, вывести в консоль все элементы массива arr
// for(let items of arr) {
//   console.log('item: ' + items);
// }

// // // Используя цикл, bывести в консоли все пары индекс:значение массива arr
// for(let i = 0; i < arr.length; i+=1) {
//   console.log(`idx:${i}  item:${arr[i]}` )
// }

/* ************************************************ */

/* task-04
  Напишите цикл, который предлагает, через prompt, ввести число больше 100. 
  
  Если посетитель ввёл другое число – попросить ввести ещё раз, и так далее.

  Цикл должен спрашивать число пока либо посетитель не введёт число, большее 100, 
  либо не нажмёт кнопку Cancel.
  
  Предполагается, что посетитель вводит только числа, обрабатывать невалидный ввод 
  вроде строк 'qweqwe' в этой задаче необязательно.
  
  PS: используйте цикл do...while
*/
/*
let inputNumber;

  do {
    inputNumber = prompt('Please, enter a number more than 100');
    if(inputNumber > 100 && inputNumber !== null ) {
      break;
    }
  } while(inputNumber !== null)
*/
/* ************************************************ */

/* task-05 finished
  Напишите скрипт, который выводит через console.log все 
  числа от min до max, с двумя исключениями: 
    
    - Для чисел, нацело делящихся на 3, вместо числа выводится строка 'Fizz'
    
    - Для чисел, нацело делящихся на 5, но не на 3, вместо числа выводится строка 'Buzz'
    
  PS: используйте цикл for
*/

// const min = 1;
// const max = 100;

// for(let i = 1; i < 100; i+=1) {
//   if(i % 3 === 0) {
//     console.log('Fizz');
//     continue;
//   }
//   if(i % 5 === 0 && i % 3 !== 0) {
//     console.log('Buzz');
//     continue;
//   }
//   console.log(i);
// }
/* ************************************************ */

/* task-06 finished
  Напишите скрипт, который выбирает из массива numbers 
  все числа, которые больше чем значение переменной num, 
  записывая эти числа в массив newArray.
      
  В результате в массиве newArray будут все подходяшие числа.
      
  PS: используйте цикл for или for...of и оператор ветвления if
*/

// const numbers = [1, 3, 17, 5, 9, 14, 8, 14, 34, 18, 26];
// const num = 10;
// const newArray = [];

// for(let number of numbers) {
//   if(number > num)
//   newArray.push(number);
// }

// console.log(numbers);
// console.log(newArray);
/* ************************************************ */

/* task-07 finished
  Напишите скрипт, который проверяет произвольную строку 
  в переменной string и находит в ней самое длинное слово,
  записывая его в переменную longestWord.
*/

// const string = "May the force be with you";
// let longestWord;
// let strArray = string.split(' ');
// let maxLength = 0;

// for(let i = 0; i < strArray.length; i++) {
//   if(strArray[i].length > maxLength) {
//      maxLength = strArray[i].length;
//     longestWord = strArray[i];
//   }
// }

// console.log(longestWord); // 'force'

/* ************************************************ */

/* task-08 finished
  Напишите скрипт который:
  
  - Запрашивает по очереди числа при помощи prompt и сохраняет их в массиве.
    Используйте do...while.
  
  - Заканчивает запрашивать числа, как только посетитель введёт не число 
    или нажмёт Cancel. При этом ноль 0 не должен заканчивать ввод, 
    это разрешённое число.
  
  - После того как ввод был завершен, если массив не пустой, 
    скрипт выводит сумму всех значений массива: "Сумма: <сумма всех значений в массиве>"
    Используйте цикл for...of
*/
/*
let inputNumber;
let numArray  = [];
let numberSum = 0;;

do{
  inputNumber = prompt('Please, enter a number');
  if(inputNumber !== null && !isNaN(inputNumber) && inputNumber !=='') {
    numArray.push(Number(inputNumber));
  } else {
    break;
  }
  console.log(numArray);
} while(true);

if(numArray.length > 0) {
  for(let number of numArray) {
    numberSum+=number;
  }
  alert(`Cумма всех значений в массиве: ${numberSum}`);
}
*/
/* ************************************************ */

/* task-09
  ***ЗАДАНИЕ ПОВЫШЕНОЙ СЛОЖНОСТИ***
  
  Создайте игру угадай число.
  
  Есть массив чисел numbers, содержащий "верные" числа.
  Числа в массиве всегда идут по возрастанию, 1-5, 20-40, 2-100 и т.п.
  
  Просим пользователя ввести цифру от самого маленького,
  до самого большого элемента массива. Эти значения необходимо
  сохранить в переменные min и max. Учтите что массив произвольный,
  но элементы всегда идут по возрастанию.
  
  Пусть prompt говорит "Введите цифру между x и y", где x и y 
  соотвественно самый маленький и самый большой элемент массива.
  
  Но пользователь может ввести что угодно, необходимо проверить 
  что было введено. Преобразовать input в числовой тип и проверить 
  число ли это.
  
    - Если не число - выводим alert с сообщением о том, что было 
      введено не число.
    - Если число - проверить содержит ли в себе массив numbers это число.
    - Если содержит - выводим alert с сообщением 'Поздравляем, Вы угадали!'.
    - Есл не содержит - выводим alert с сообщением 'Сожалеем, Вы не угадали!'.
*/



// const numbers = [12, 15, 25, 37, 41];

// let min = numbers[0];
// let max = numbers[numbers.length-1]


// let inputNum;

// do {
//   inputNum = prompt(`Введите цифру между ${min} и ${max}`);
//   if(isNaN(inputNum) || inputNum === '' ) {
//     alert('Введите число');
//   } else if (inputNum === null) {
//     break;
//   } else if (numbers.slice(1, numbers.length - 1).includes(Number(inputNum))){
//     alert('Поздравляем, Вы угадали!');
//     break;
//   } else {
//     alert('Сожалеем, Вы не угадали!');
//     break;
//   }
// } while(true);
