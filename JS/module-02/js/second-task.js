/*
  ⚠️ ЗАДАНИЕ ПОВЫШЕННОЙ СЛОЖНОСТИ - ВЫПОЛНЯТЬ ПО ЖЕЛАНИЮ
  
  Напишите скрипт авторизации пользователя.
  
  Есть массив паролей зарегистрированных пользователей passwords. 
  
  При посещении страницы, необходимо попросить пользователя ввести свой пароль,
  после чего проверить содержит ли массив passwords пароль введенный пользователем.
  
  Пароль можно ввести не верно всего n раз, кол-во хранится в переменной attempts.
  Подсказка: используйте цикл do...while.
  Если был введен пароль который есть в массиве passwords, вывести alert 
  с текстом 'Добро пожаловать!' и прекратить спрашивать пароль в цикле.
  Если был введен не существующий пароль, отнять от лимита попыток единицу, 
  вывести alert с текстом "Неверный пароль, у вас осталось n попыток", 
  где n это оставшийся лимит. 
  
  После того как пользователь закрыл alert, запросить пароль снова. 
  Продолжать запрашивать пароль до тех пор, пока пользователь не введет 
  существующий пароль, не кончатся попытки или пока пользователь 
  не нажмет Cancel в prompt.
  Если закончились попытки, вывести alert с текстом "У вас закончились попытки, аккаунт заблокирован!"
  
  Если пользователь нажмет Cancel, прекратить выполнение цикла.
*/

const passwords = ['qwerty', '111qwe', '123123', 'r4nd0mp4zzw0rd'];
let attempts = 3;
let InputPasswors;

do {
    InputPasswors = prompt('Please, enter your password');
    if(passwords.includes(InputPasswors)) {
        alert('Добро пожаловать!');
        break;
    } else if(InputPasswors !== null){
        if(attempts === 0) {
            alert('"У вас закончились попытки, аккаунт заблокирован!"');
            break;
        }
        alert(`Неверный пароль, у Вас осталось ${attempts--} попыток`)
    }
}while(InputPasswors !== null);


function addUser(name, age) {
    event.preventDefault();
    fetch('https://test-users-api.herokuapp.com/users', {
        method: 'POST',
        body: JSON.stringify({'name': name, 'age': age}),
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
    }
})
    .then(request => {
        if(request.ok) return request.json();
        throw new Error(`POST function error# ${request.status}`)
    })
    .then(x => {
        list.insertAdjacentHTML('beforeend', `<li class='succsess'>New user added<br>ID: ${x.data._id}<br>NAME: ${x.data.name}<br>AGE: ${x.data.age}<li>`);
    })
    .catch(error => list.insertAdjacentHTML('beforeend',`<li class="error">${error}<li>`));
}


function removeUser(id) {
    event.preventDefault();
}


function getData(callback) {
    while(true) {
        let name = prompt('Please, enter a name');
        if(name === '' || name === null) {
            alert('string with name cannot be empty, try again')
            break;
        }
        let age = Number.parseInt(prompt('Please, enter an age'));
        if(age % 1 !== 0 || age === null) {
            console.log(age);
            alert('age must be a number, try again')
            break;
        }
        if(name.length > 0 && age % 1 === 0) {
            callback(name, age);
            break;
        }
    }
}

