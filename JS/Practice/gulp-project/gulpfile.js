var gulp = require('gulp');
const sass = require('gulp-sass');
const runSequence = require('run-sequence');
const autoprefixer = require('gulp-autoprefixer');
const cssnano = require('gulp-cssnano');
const rigger = require('gulp-rigger');
const imagemin = require('gulp-imagemin');
const browserSync = require('browser-sync').create();

const path = {
    src: {
        html: './src/html/index.html',
        scss: './src/scss/style.scss',
        css: './build'
    },

    dest: {
        html: './build',
        scss: './build'
    }
}

gulp.task('html', () => (
    gulp.src(path.src.html)
    .pipe(rigger())
    .pipe(gulp.dest(path.dest.html))
    .pipe(browserSync.stream())
));


gulp.task('scss',()=>(
    gulp.src(path.src.scss)
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
        browsers: ['last 5 versions'],
        cascade: false
        }))
        .pipe(cssnano())
        .pipe(gulp.dest(path.dest.scss))
        .pipe(browserSync.stream())
));


gulp.task('img', () => 
gulp.src('./src/imgs/*')
    .pipe(imagemin())
    .pipe(gulp.dest('./build/imgs/*'))
);

gulp.task('fonts', () => (
    gulp.src('./src/fonts/*')
    .pipe(gulp.dest('./build/fonts/*'))
));


gulp.task('browser-sync', () => (
    browserSync.init({
        server: {
            baseDir: "./build"
        }
    })
));

gulp.task('watch',  () => {
    gulp.watch('./src/html/*.html', ['html']);
    gulp.watch('./src/scss/*.scss', ['scss']);
});

gulp.task('build', function(callback) {
    runSequence('html', 'scss', 'img', 'fonts', 'browser-sync', 'watch', callback);
  });