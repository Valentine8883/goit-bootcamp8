/*

<footer>
  <div id="icons">
    <a href="https://www.linkedin.com/in/joni-vainio-kaila-924726143/" target="_blank" class="fa fa-linkedin"></a>
  </div>
  <p id="copyright">Tribute page project for <i class="fa fa-free-code-camp"></i> by Joni Vainio-Kaila</p>
</footer>
`
*/

function buildElement(elementName, attrName, attrValue) {
  const element = document.createElement(elementName);
  if(attrName !== undefined) { element.setAttribute(attrName, attrValue)};
  return element;
}

function buildSection(className = 'no data', pTextContent = 'no data',h3TextContent = 'no data', h3DivTextContent = 'no data') {
  const section = document.createElement('section');
  const div = buildElement('div', 'class', className);
  const h3 = document.createElement('h3');
  h3.textContent = h3TextContent
  const h3Div = buildElement('div', 'class', 'year');
  h3Div.textContent = h3DivTextContent;
  h3.appendChild(h3Div);
  const p = document.createElement('p');
  p.textContent = pTextContent;
  section.append(div, h3, p);
  return section;
}


const main = document.createElement('main');
main.id = 'main';

const firstRowDiv = document.createElement('div');
firstRowDiv.id = 'first-row';
main.appendChild(firstRowDiv);

const imgDiv = document.createElement('div');
imgDiv.id = 'img-div';
firstRowDiv.appendChild(imgDiv);

const figure = document.createElement('figure');
imgDiv.appendChild(figure);

const figureImg = document.createElement('img');
figureImg.id = "image";
figureImg.src = "http://jonivainio-kaila.fi/freecodecamp/tribute/assets/img/einstein-mobile.jpg";
figureImg.alt = "Profile picture of Albert Einstein.";

const figureFigcaption = document.createElement('figcaption');
figureFigcaption.setAttribute('id', "img-caption");
figureFigcaption.textContent = 'Profile picture of Albert Einstein';

figure.append(figureImg, figureFigcaption);

const tributeInfo = buildElement('div', 'id', "tribute-info");
firstRowDiv.appendChild(tributeInfo);

const tributeInfoP  = document.createElement('p');
tributeInfoP.innerHTML = `<strong>Albert Einstein</strong> was a German-born theoretical physicist. He developed the theory of relativity, one of the two pillars of modern physics. Einstein's work is also known for its influence on the philosophy of science. Einstein is
best known in popular culture for his mass-energy equivalence formula. He received the 1921 Nobel Prize in Physics for his services to theoretical physics, and especially for his discovery of the law of the photoelectric
effect, a pivotal step in the evolution of quantum theory`

tributeInfo.appendChild(tributeInfoP);

const quoteDiv = buildElement('div', 'id', "quote");
main.appendChild(quoteDiv);

const blockquote = document.createElement('lockquote');
blockquote.textContent = "Anyone who has never made a mistake has never tried anything new";

quoteDiv.appendChild(blockquote);

const timelineDiv = buildElement('div', 'id', "timeline");
main.appendChild(timelineDiv);

const timelineDivH = document.createElement('h2');
timelineDivH.textContent = "Timeline of Albert Einstein's life";

timelineDiv.appendChild(timelineDivH);

const sectionEnstain = buildSection("fa fa-mars", "Albert Einstein is born in Ulm, Germany, the son of Hermann Einstein, a German Jewish featherbed salesman, and his wife Pauline.", "Albert Einstein born", '1879');
const sectionMagnet = buildSection("fa fa-magnet", "At the age of five, Albert Einstein becomes fascinated by his father's pocket compass, intrigued by invisible forces that cause the needle always to point north. Later in life, Einstein will look back at this moment as the genesis of his interestin science.","Mystery of Magnetism",'1884');
const sectionItaly = buildSection("fa fa-plane", "Struggling financially, the Einstein family moves from Germany to Italy in search of better work. Albert, aged fifteen, stays behind in Munich to finish his schooling, but soon either quits or is kicked out of his high school and follows his parents", "Move to Italy",'1894');
const sectionSchool = buildSection("fa fa-ban", `Albert Einstein attempts to get out of his last year of high school by taking an entrance exam to ETH, the Swiss Polytechnic University in Zurich. He fails the test, forcing him to attend one final year of high school in the small town of Aarau`, 'Boarding School in Aarau','1895');
const sectionETH = buildSection("fa fa-university", "Albert Einstein graduates from high school and begins attending ETH, the prestigious Swiss Polytechnic University in Zurich.", 'Einstein at ETH','1896');
const sectionCollege = buildSection("fa fa-graduation-cap", "Albert Einstein graduates from ETH with a degree in physics. He tries to find a teaching job, but is unable to obtain work.", 'College Graduation', '1900');
const sectionSwiss = buildSection("fa fa-briefcase", "Unable to find any work as a teacher or academic, Albert Einstein takes a job as a clerk at the Swiss Patent Office.", 'Swiss Patent Office', '1902');
timelineDiv.append(sectionEnstain, sectionMagnet, sectionItaly, sectionSchool, sectionETH, sectionCollege, sectionSwiss);


const linkDiv = buildElement('div', 'id', 'link');
main.appendChild(linkDiv);

const linkDivA =  buildElement('a', 'id', 'tribute-link');
linkDivA.href="http://www.shmoop.com/albert-einstein/timeline.html";
linkDivA.textContent = 'Full timeline at Shmoop!';

linkDivA.target="_blank";

linkDiv.append(linkDivA);


document.querySelector('#root').append(main);







