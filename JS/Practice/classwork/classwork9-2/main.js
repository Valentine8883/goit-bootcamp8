'use strict';

// Ajax 
/*
// https://jsonplaceholder.typicode.com/
const req = new XMLHttpRequest();
// console.log(req);
// settings
// req.open(method, URL, async);
req.open('GET', 'https://jsonplaceholder.typicode.com/users', false); // открывает соеденение с ресурсом 
//get\ post \delete\ put методы запросов
//true - асинхронный  false - синхронный
req.setRequestHeader('Content-Type', 'application/json');  //доп. настройки запроса
// req.send([body])
req.send();
// req.abort()
// status
// https://developer.mozilla.org/ru/docs/Web/HTTP/Status
if (req.status !== 200) {
  console.error(`${req.status}: ${req.statusText} `)
} else {
  console.log(JSON.parse(req.response));
}
console.log(`test`);
*/
/*
const req = new XMLHttpRequest();
req.open('GET', 'https://jsonplaceholder.typicode.com/users', true);
// req.setRequestHeader('Content-Type', 'application/json');
req.send();
req.onreadystatechange = function () { // функция следит за состоянием запроса
    if (req.readyState !== 4) {  // состояние запроса в req.readystate 0 - початок // 1 - викликаний open// 2 - отримані заголовки// 3 - отримуємо данні// 4 - запит закінчено
        console.log(req.readyState);
        return
    }

    if (req.status !== 200) {
        console.error(`${req.status}: ${req.statusText} `)
    } else {
        console.log(JSON.parse(req.response)); // JSON.parse()
    }
};
*/
/*
const body = document.body;

const list = document.createElement('ul');
body.append(list);

function createItem(name) {
    let item = document.createElement('li');
    item.textContent = name;
    return item;
 }

fetch('https://jsonplaceholder.typicode.com/users')// по дефолту GET!
.then(response => response.json())
.then(data => data.forEach(x => list.append(createItem(x.name))))// параметр функции

// CRUD
// POST - CREATE
// GET - READ
// PUT - UPDATE
// DELETE - DELETE
*/


const body = document.body;
const list = document.createElement('.container');
body.append(list);
function createItem(src) {
    const div = document.createElement('div');
    div.classList.add('one');
    const img = document.createElement('img');
    const p = document.createElement('p');
    div.append(p, img);
    img.src = src
    return div;
 }
const key = '5018958-ed49ccd90878e6614abdf24a6';
let url = `https://pixabay.com/api/?key=${key}&order=latest&per_page=9&q=cat`;
fetch(url)
.then(respons => respons.json())
.then(data => data.hits.forEach(x => list.append(createItem(x.largeImageURL))));






// function createImage() {
//     const div = document.createElement('div');
//     div.classList.add('one');
//     const img = document.createElement('img');
//     const p = document.createElement('p');
//     div.append(p, img);
    
// }

// const key = '5018958-ed49ccd90878e6614abdf24a6';
// let url = `https://pixabay.com/api/?key=${key}&category=backgrounds&order=latest&per_page=9&q=cat`;
// fetch(url)
// .then(respons => respons.json())
// .then(data => data.hits.forEach(x => list.append(createItem(x.largeImageURL))));



