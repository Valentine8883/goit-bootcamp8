'use strict';

const email = document.querySelector('.js-input-email');
const btn = document.querySelector('.js-button');

let pattern = /^\w{1,}@\w{1,5}\.com/g

btn.addEventListener('click', valid);

function valid() {
    event.preventDefault();
    console.log(email.value);
    if(pattern.test(email.value)) {
        console.log('valid')
    } else {
        console.log('not valid');
    }
}

// const reg = new RegExp(); // способ создания регулярых выражение 
// const pattern = /win/; // создание через литерал

// const string = 'text windows win';


// console.log(string.match(pattern)); // если нет возвращает null
// console.log(pattern.test(string)); // возвращает true or false


// // $flag
// let string = 'Is that all there is?',
// reg = /is/gi; // g - глобально ищет совпадения во всей строке c учетом регистра
// // i - игнорировать регистр
// console.log(string.match(reg));


// //$classes
// let string = '12345 q5 wq w. wwwh - dfsdf',

// reg = /\W/g; // d - цифры в середине строки(digit) // D - все кроме чисел // w - цифры и буквы без спецсимволов // W - спецсимволы

// console.log(string.match(reg));


// //$quanitity

// let string = '9234543 123, 243 467 52 34 987 5';
// // let regV = /\b\d{2,}/g // {n} - выбирает первые три n цифр // {n,m} - от n - min m - max count decimal
// let regV = /\b\d{3,}\b/g // /b - выделфет слово
// console.log(string.match(regV));


// // $sets 

// let string = 'random text with a number - 5',
// regV = /[a-zA-Z]/ig;  // [] - наборы символов
// console.log(string.match(regV));

//$boundaries

// let string = 'gjhjkhkhjkh 44rer.js', 
// regV = /^\d{2}$/; // ^ - начало строки $ - enddof string

// // . - любой символ


//s - обрабатывает пробелы

// Флаги
// i - ігнорує регістр*
// g - глобальний погук а не перший збіг*
// m - багатостроковий пошук*
// Класи
// \d - numbers 0-9*
// \D - not numbers*
// \w - word*
// \W - not word*
// \s - space*
// \S - not space*
// \b - word or symbol with spase around*
// \B - not word*
// [] - набір *
// - діапазон *
// ^ - в [] це виключення за межами набору це початок строки*
// {} - показник кількості*
// () - група символів* // /\b(с|g|p)[a-z]{1,}\b/g
//  + - один або більше
//  ? - нуль або один   //  сымвол может отсутствовать, если есть то максимум один
//  * - нуль або більше
//  . - будь - який символ*
//  $ - кінець строки*































































































// event loop




// let date = new Date();
// console.log(date);
// console.log(date.getDate());
// console.log(date.getMonth());
// console.log(date.getFullYear());
// console.log(date.getDay());





















// let intervalID = setInterval(()=> console.log('hello'), 1000);
// console.log(intervalID);

// clearInterval(intervalID);


//clearTimeout()

//min delay 4 ms























// let date = new Date(1000000);
// console.log(date);

// let date = Date.now();
// console.log(date); // 1521471595501

// let date = new Date();
// console.log("Date: " + date);
// console.log("getDate(): " + date.getDate());
// console.log("getDay(): " + date.getDay());
// console.log("getMonth(): " + date.getMonth());
// console.log("getFullYear(): " + date.getFullYear());
// console.log("getHours(): " + date.getHours());
// console.log("getMinutes(): " + date.getMinutes());
// console.log("getSeconds(): " + date.getSeconds());
// console.log("getMilliseconds(): " + date.getMilliseconds());
// console.log("getTime(): " + date.getTime());































// function User(name, surname) {
//     this.name = name;
//     this.surname = surname;
// }

// User.prototype.getFullName = function() {
//     return `Name: ${this.name}\nSurname: ${this.surname}`;
// };

// function Student(name, surname, yearIn) {
//     User.call(this, name, surname);
//     this.yearIn = yearIn;
// };

// Student.prototype = Object.create(User.prototype);
// Student.prototype.constructor = Student;

// Student.prototype.getCourse = function() {
//     return `${2018 - this.yearIn}`;
// };

// Student.prototype.getFullInfo = function() {
//     console.log(`${this.getFullName()}\nCourse: ${this.getCourse()}`);
// };

// const polly = new Student('Polly', 'Smith', 2016);

// polly.getFullInfo();
// console.log(polly);


// Part 1
// Створити конструктор Book який створює обєкти такого типу {title: title, author:author, pages: pages, price: price, curentPage: 0};
// Помістити в прототип метод  buy(arr) який приймає аргументом масив order додає ціну книжки в нього.
// Помістити в прототип метод read(num) який приймає аргументом кількість сторінок що прочитано і змінює значення поля curentPage;

// Створити 3 обєкта книжки викликати для них методи buy() та read(num); 
// Порахувати загальну вартість замовлення з масива order.
// Вивести в консоль змінене значення поля curentPage після запуску методів read(num); 
// Всі куплені книги додати в масив books.
// Part 3 
// Створити корструктор PapperBook який наслідує всі властивості консруктора Book і додає до обєкта книги додаткове поле cover.
// Додати в прототип PapperBook метод burn(arr) який приймає аргументом масив books з частини №1 при виклику виводить в консоль напис 'Book {title} has been burned' і видаляє з масива books ту книгу для якої викликаємо цей метод.
// Створити 3 книги конструктором PapperBook і викликати для них метод buy(arr) щоб додати їх до масиву order.
// Викликати для першої і другої книги методи burn(arr), щоб видатити їх з масива books

// let books = [];
// let order = [];

// function Book(title, autor, pages, price) {
//     this.title = title;
//     this.autor = autor;
//     this.pages = pages;
//     this.price = price;
//     this.curentPages = 0;
// }

// Book.prototype.buy = function(arr) {
//     arr.push(this.price);
// }

// Book.prototype.read = function(num) {
//     this.curentPages = num;
// }

// const book1 = new Book('Art of War', 'Sun Czi', 100, 30);
// book1.buy(order);
// book1.read(30);
// console.log('Curent page: ' + book1.curentPages);

// const book2 = new Book('Holmes', 'Konan doel', 150, 50);
// book2.buy(order);
// book2.read(40);
// console.log('Curent page: ' + book2.curentPages);

// const book3 = new Book('World war2', 'Winston Cherchil', 500, 300);
// book3.buy(order);
// book3.read(15);
// console.log('Curent page: ' + book3.curentPages);

// console.log(order);
// console.log("Order: " + order.reduce((acc, x)=> acc + x));

// books.push(book1, book2, book3);
// console.log(books);

// //Part 2
// // Створити ф-ю констпуктор E-Book який наслідує всі властивості консруктора Book і додає до обєкта книги додаткове поле fileSize де вказується розмір книги в кілобайтах.
// // Додати в прототип E-Book метод download() який при виклику виводить в консоль напис 'Book {title} has been dowloaded, app was downloaded {fileSize} kb';
// // Створити 2 книги конструктором E-Book і викликати для них метод buy(arr) щоб додати їх до масиву order.
// // Викликати для них метод download()


// function EBook(title, autor, pages, price, fileSize) {
//     Book.call(this, title, autor, pages, price);
//     this.fileSize = fileSize;
// }

// EBook.prototype = Object.create(Book.prototype);
// EBook.prototype.constructor = EBook;

// EBook.prototype.download = function() {
//     console.log(`Book ${this.title} has been dowloaded, app was downloaded ${this.fileSize} kb`);
// }

// const ebook1 = new EBook('EBOOK Art of War', 'Sun Czi', 100, 30, 300);
// ebook1.buy(order);
// ebook1.download();

// const ebook2 = new EBook('EBOOK Holmes', 'Konan doel', 150, 50, 300, 240);
// ebook2.buy(order);
// ebook2.download();

// console.log(ebook1);

// // Part 3 
// // Створити корструктор PapperBook який наслідує всі властивості консруктора Book і додає до обєкта книги додаткове поле cover.
// // Додати в прототип PapperBook метод burn(arr) який приймає аргументом масив books з частини №1 при виклику виводить в консоль напис 
// //'Book {title} has been burned' і видаляє з масива books ту книгу для якої викликаємо цей метод.
// // Створити 3 книги конструктором PapperBook і викликати для них метод buy(arr) щоб додати їх до масиву order.
// // Викликати для першої і другої книги методи burn(arr), щоб видатити їх з масива books



// function PapperBook(title, autor, pages, price, cover) {
//     Book.call(this, title, autor, pages, price);
//     this.cover = cover;
// } 

// PapperBook.prototype = Object.create(Book.prototype);
// PapperBook.prototype.constructor = PapperBook;

// PapperBook.prototype.burn = function(arr,) {
//     arr.splice(arr.indexOf(this.title), 1);
//     console.log(`Book ${this.title} has been burned`)
// }


// const ppbook1 = new PapperBook('title1', 'autor1', 100, 200, 'paper');
// ppbook1.buy(order);
// const ppbook2 = new PapperBook('title2', 'autor2', 100, 200, 'paper');
// ppbook2.buy(order);
// const ppbook3 = new PapperBook('title3', 'autor3', 100, 200, 'paper');
// ppbook3.buy(order);

// books.push(ppbook1, ppbook2, ppbook3);

// console.log(books);

// ppbook1.burn(books);

// console.log(books);





// function Human(name, age, gender) {
//     this.name = name;
//     this.age = age;
//     this. gender = gender;
// }

// Human.prototype.fullInfo = function() {
//     console.log(`i am ${this.name}`);
// };

// Human.prototype.speak = function(words) {
//     console.log(`${this} say word: ${words}`);
// };

// Human.prototype.walk = function() {
//     console.log(`${this} can walk`);
// };

// Human.prototype.breath = function() {
//     console.log(`${this.name} can breathe`);
// };

// let polly = new Human('Polly', 1, 'female');
// let mango = new Human('Mango', 2 , 'male');

// console.log(polly);
// console.log(mango);
































// class Student {constructor (){}}
// console.log(typeof Student === 'function');


// const Hero = function(name, level) {
//   this.name = name;
//   this.level = level;
// }

// Hero.prototype.greet = function() {
//   console.log(`Hello I'm a ${this.name}`);
// }

// const Warrior = function(name, level, weapon) {
//   Hero.call(this, name, level);
//   this.weapon = weapon;
// }
// Warrior.prototype = Object.create(Hero.prototype);
// Warrior.prototype.constructor = Warrior;

// Warrior.prototype.attack = function() {
//   console.log(`${this.name} attack width ${this.weapon}`);
// }



// const superman = new Warrior('superman', 999, 'laser');

// superman.attack();
// superman.greet();


// const Berserker = function(name, level, weapon, shout) {
//   Warrior.call(this, name, level, weapon);
//   this.shout = shout;
// }

// Berserker.prototype = Object.create(Warrior.prototype);
// Berserker.prototype.constructor = Berserker;
// Berserker.prototype.aaaa = function() {
//   console.log('aaaaaaaaaæ');

// }


// const ber = new Berserker('berserker', 56, 'sward', 'dsddss' ); 

// ber.attack();

// const x = new Object();
// x.name = 'x'; 
// console.log(x.__proto__);

// const y = Object.create(x);

// y.name = 'y';

// const b = Object.create(y);
// b.name = 'b';
// console.log(b);

// function show() {
//   console.log('show function');
// }

// show();

// const inventors = [{
//     first: 'Albert',
//     last: 'Einstein',
//     year: 1879,
//     passed: 1955
//   },
//   {
//     first: 'Isaac',
//     last: 'Newton',
//     year: 1643,
//     passed: 1727
//   },
//   {
//     first: 'Galileo',
//     last: 'Galilei',
//     year: 1564,
//     passed: 1642
//   },
//   {
//     first: 'Marie',
//     last: 'Curie',
//     year: 1867,
//     passed: 1934
//   },
//   {
//     first: 'Johannes',
//     last: 'Kepler',
//     year: 1571,
//     passed: 1630
//   },
//   {
//     first: 'Nicolaus',
//     last: 'Copernicus',
//     year: 1473,
//     passed: 1543
//   },
//   {
//     first: 'Max',
//     last: 'Planck',
//     year: 1858,
//     passed: 1947
//   },
//   {
//     first: 'Katherine',
//     last: 'Blodgett',
//     year: 1898,
//     passed: 1979
//   },
//   {
//     first: 'Ada',
//     last: 'Lovelace',
//     year: 1815,
//     passed: 1852
//   },
//   {
//     first: 'Sarah E.',
//     last: 'Goode',
//     year: 1855,
//     passed: 1905
//   },
//   {
//     first: 'Lise',
//     last: 'Meitner',
//     year: 1878,
//     passed: 1968
//   },
//   {
//     first: 'Hanna',
//     last: 'Hammarström',
//     year: 1829,
//     passed: 1909
//   }
// ];

// console.log('1) Отримати масив вчених що народилися в 19 ст'); // finished
// console.log(inventors.filter(x => x.year > 1800 && x.year < 1900));

// console.log('2) знайти суму років скільки прожили всі вченні'); //finished
// console.log(inventors.reduce((acc, x) => acc + (x.passed - x.year), 0));

// console.log('3) Відсортувати вчених по алфавіту'); //finished
// console.log(inventors.sort((a, b) => a.first > b.first? 1: -1));

// console.log('4) Відсортувати вчених по даті народження'); //finished
// console.log(inventors.sort((a, b) => a.year - b.year));

// console.log('5) Відсортувати вчених по кількості прожитих років'); // finished
// console.log(inventors.sort((a, b) => (a.passed-a.year) - (b.passed-b.year)));

// console.log('6) Видалити з масива вчених що родилися в 15 або 16 або 17 столітті')  //finished
// console.log(inventors.filter(x => x.year >= 1800));

// console.log('7) Знайти вченого який народився найпізніше.'); //finished
// console.log(inventors.find(x => Math.max(x.year)));

// console.log('8) Знайти рік народження Albert Einstein'); //finished
// console.log(inventors.filter(x => x.last === 'Einstein').map(x => x.year));

// console.log('9) Знайти вчених прізвище яких починається на літеру С'); //finished
// console.log(inventors.filter(x => x.last[0] === 'C'));

// console.log('10) Видалити з масива всіх вчених імя яких починається на A'); //finished
// console.log(inventors.filter(x => x.first[0] !== 'A'));


// let arr1 = [1, 2, 3, 4, 5];

// const userDb = [
//     {name: 'Polly', isActive: true, friends: 34},
//     {name: 'Jimmy', isActive: false, friends: 25},
//     {name: 'Alex', isActive: true, friends: 4},
//     {name: 'Ajax', isActive: false, friends: 98},
// ]

// const map = function(arr, callback)  {
//     let newArray = [];
//     for(let i=0; i < arr.length; i+=1) {
//         let item = callback(arr[i]);
//         newArray.push(item);
//     }
//     return newArray;
// }


// console.log(map(userDb, user => user.friends));
// console.log(map(userDb, user => user.name));
// console.log(map(userDb, user => user.isActive = !user.isActive));
// console.log(map(userDb, user => ({...user, isActive :!user.isActive})));


// console.log(userDb.map(x => x.name));
/**********************************************************************/

// function counter() {
//     let count = 0;
//     function increment() {
//         count++;
//         console.log(count);
//     }
//     return increment;
// }

// let user = counter();


// user();
// user();
// user();

/**********************************************************************/

// const hotel = {
//     name: "Premium",
//     stars: 5,
//     status: 'lux',
//     capacity: 100,

// }

// console.log(hotel.name);
// console.log(hotel['name']);


// let user = {
//     name: 'bob',
//     age: 35
// }

// let user2 = {
//     name1: 'bob',
//     age1: 35
// }
// // let user2 = Object.assign({ageee: '1'}, user);

// // console.log(user);

// // console.log(user2);


// const c = {
//     ...user,
//     ...user2,
//     age: '12'
// }

// console.log(c);

// const{name, age} = user;

// console.log(user)

/**********************************************************************/
/*
написати ф-ю getSalary(worker, rate) яка приймає аргументами обєкт робітника і погодинну ставку.
Ф-я вертає зарплату робітника за тиждень з урахуванням штрафів і премій
hours - години роботи
value - сума штрафу
premium - премія
console.log(getSalary(worker, 20)); // 1235
console.log(getSalary(worker, 8)); // 671
*/

// let worker = {
//     workSchedule: [
//         {day: 'M', hours: 9},
//         {day: 'T', hours: 10},
//         {day: 'W', hours: 11},
//         {day: 'T', hours: 5},
//         {day: 'F', hours: 12},
//         ],
//         penalty: [
//         {day: 'M', value: 0},
//         {day: 'T', value: 120},
//         {day: 'W', value: 50},
//         {day: 'T', value: 0},
//         {day: 'F', value: 35},
//         ],
//         premium: 500,
// }

// function getSalary(obj, rate) {
//     let sumH = 0;
//     let sumV = 0;
//     for(let item of obj['workSchedule']) {
//         sumH += item['hours'];
//     }
//     for(let item of obj['penalty']) {
//         sumV += item['value'];
//     }
//     return sumH * rate + obj['premium'] - sumV;
    
// }

// console.log(getSalary(worker, 20)); // 1235
// console.log(getSalary(worker, 8)); // 671

// /**********************************************************************/
// ///Дано обєкт товарів виведіть товари в консоль ціна яких більше 20


// let goods = {
// 	"dd" : {
// 		"name": "Яблоки",
// 		"cost": 13
// 	},
// 	"dc" : {
// 		"name": "Груши",
// 		"cost": 23
// 	},
// 	"cd" : {
// 		"name": "Абрикосы",
// 		"cost": 33
// 	},
// 	"dee" : {
// 		"name": "Сливы",
// 		"cost": 43
// 	},
// 	"dd2" : {
// 		"name": "Вишни",
// 		"cost": 19
// 	},
// };

// function moreThan(obj) {
//     for(let key in obj) {
//         if(obj[key].cost > 20)
//         console.log(obj[key]);
//     }
// }
// moreThan(goods);

// /**********************************************************************/

