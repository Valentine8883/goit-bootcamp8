import EventEmitter from '../services/event-emitter';


export default class View extends EventEmitter{
    constructor() {
        super();
        this.form = document.querySelector('.form');
        this.input = this.form.querySelector('.input');
        this.notesGrid = document.querySelector('.notes-grid');
        this.form.addEventListener('submit', this.handleAdd.bind(this));
    }

    handleAdd(evt) {
        evt.preventDefault();
        const {value} = this.input;
        if(value === '') {
            alert('String cannot be empty');
            return;
        }
        this.emit('add', value);
    }

    createNote(note) {
        const item = document.createElement('div');
        item.classList.add('item');
        item.dataset.id = note.id;

        const text = document.createElement('p');
        text.classList.add('text');
        text.textContent = note.text;

        const buttonRemove = document.createElement('button');
        buttonRemove .classList.add('button');
        buttonRemove .dataset.action = 'remove'
        buttonRemove .textContent = 'Remove';

        const buttonEdit = document.createElement('button');
        buttonEdit.classList.add('button');
        buttonEdit.dataset.action = 'etit'
        buttonEdit.textContent = 'Edit';

        item.append(text, buttonRemove, buttonEdit);

        this.appendEventListeners(item);
        return item;
    }

    addNote(note) {
        const item = this.createNote(note);
        this.form.reset();
        this.notesGrid.appendChild(item);

    }

    appendEventListeners(item) {
        const removeBtn = item.querySelector('[data-action="remove"]');
        removeBtn.addEventListener('click', this.handleRemove.bind(this));
    }

    handleRemove({target}) {
        const parent = target.closest('.item');
        this.emit('remove', parent.dataset.id);
    }

    removeNote(id) {
        const items = this.notesGrid.querySelectorAll('.item');
        items.forEach(x => x.dataset.id === id ? x.remove() : null);

    }



}