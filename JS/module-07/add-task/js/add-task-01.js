/*
  Есть список категорий с классом categories (на вкладке HTML).
  
  Напишите код, который для каждого элемента li (первая вложенность) 
  в списке categories выведет в консоль:
  - Текст непосредственно в нём (название категории)
  - Количество всех вложенных в него элементов li
  
  К примеру:
    Категория: Животные
    Количество вложенных li: 4
*/

// for(let i = 0; i < 3; i += 1) {
//   console.log(document.querySelector('.categories').children[i].firstChild.textContent.trim());
//   console.log(document.querySelector('.categories').children[i].firstElementChild.childElementCount);
// }

// for(let i = 0; i < item.length; i+=1) {
//   console.log(item[i].);
//   console.log(item[i].firstElementChild.childElementCount);
// }

const item = document.querySelectorAll('.categories > li');
item.forEach(item => console.log(item.firstChild.textContent.trim(), item.firstElementChild.childElementCount));

console.log(document.querySelectorAll('.categories > li'));