/*
  Дан список с классом .list
	- Найдите первого потомка списка и сделайте его текст красного цвета
	- Найдите последнего потомка списка и сделайте его текст синего цвета
*/

document.querySelector('.list').firstElementChild.style.color = 'red';
document.querySelector('.list').lastElementChild.style.color = 'blue';
