/*
  Создайте функцию createPostCard(), которая 
  создает и возвращает DOM-узел карточки поста.
  
  Разметка с классами есть на вкладке HTML.
  Стили на вкладке CSS.
  
  Используйте createElement для создания узлов.
  Добавьте классы и атрибуты.
*/

function produceElement(tagName, className) {
    const element = document.createElement(tagName);
    element.classList.add(className);
    return element;
}

function createPostCard() {
    const rootDiv = produceElement('div', 'post')
  
    const img = produceElement('img', 'post__image');
    img.src = "http://via.placeholder.com/400x150";
    img.alt = "post image"

    const title = produceElement('h2', 'post__title');
    title.textContent = 'Lorem ipsum dolor';

    const paragraph = produceElement('p', 'post__text');
    paragraph.textContent = 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga, nemo dignissimos ea temporibus voluptatem maioresmaxime consequatur impedit nobis sunt similique voluptas accusamus consequuntur, qui modi nesciunt veritatis distinctio rem!'

    const btn = produceElement('a', 'button');
    btn.href = '#';
    btn.textContent ='Read more';

    rootDiv.append(img, title, paragraph, btn);
    document.body.appendChild(rootDiv);
}

createPostCard();