/*
  В HTML-документе уже есть тег с id="root" (вкладка HTML)
  
  Создайте функцию createBoxes(num), которая принимает 1 параметр num - число.
  
  Функция создает столько div, сколько указано в num и возвращает их в одном
  общем контейнере. После чего необходимо повесить результат работы функции
  в div с id="#root"
  
  Каждый div:
    - Имеет случайный rgb цвет фона
    - Размеры самого первого div - 30px на 30px.
    - Каждый следующий div после первого, должен быть шире и выше предыдущего
      на 10px
*/

function createBoxes(num){
  const conteiner = document.createElement('div');
  for(let i=0; i < num; i++) {
    let div = document.createElement('div');
    div.style.backgroundColor = `rgb(${parseInt(Math.random()*255)}, ${parseInt(Math.random()*255)}, ${parseInt(Math.random()*255)})`;
    div.style.width = `${i*10+30}px`;
    div.style.height = `${i*10+30}px`;
    div.style.margin = 'auto';
    conteiner.appendChild(div);
  }
  return conteiner;
}

document.querySelector('#root').appendChild(createBoxes(10000));
