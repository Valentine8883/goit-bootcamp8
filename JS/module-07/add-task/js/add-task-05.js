/*
  Есть список с классом .size-filter из произвольного 
  количества чекбоксов, каждый из которых содержит 
  размер одежды в фильтре.
  
  Напишите функцию getInputsData(inputs), которая
  принимает 1 параметр inputs - массив тех инпутов
  у которых состояние checked.
  
  Возвращает массив значений атрибута value.
*/

// const arr = [];
// document.querySelectorAll('.size-filter input').forEach(x => x.hasAttribute('checked')? arr.push(x) :null);


// function getInputsData(inputs) {
//   let innerArr = inputs.map(x => x.getAttribute('value'));
//   return innerArr;
// }
// console.log(getInputsData(arr));





function getInputsData(inputs) {
  let innerArr = [];
  inputs.forEach(x => innerArr.push(x.getAttribute('value')));
  return innerArr;
}

const inputArray = document.querySelectorAll('.size-filter input[checked]');
console.log(getInputsData(inputArray));