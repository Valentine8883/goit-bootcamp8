/*
  Напишите скрипт для создания списка ul.
  
  Для каждого пункта:
    - Запрашивайте содержимое пункта li у пользователя с помощью prompt.
    - Создавайте пункт и добавляйте его к ul.
    - Процесс прерывается, когда пользователь нажимает Cancel.
    - Все элементы списка должны создаваться динамически.
*/

// do {
//     let li = prompt('Please, enter an item');
//     if(li === null) {break};
//     document.querySelector('.list').innerHTML += `<li>${li}</li>`;
// } while(true);

// function createItem(itemContent = 'item', selector = '.list') {
//     let list = document.querySelector(selector);
//     let item = document.createElement('li');
//     item.textContent = itemContent;
//     list.append(item);  
// }

// while(true) {
//     let input = prompt('please, enter an item');
//     if(input === null) {break};
//     createItem(input, '.list');
// }

const list = document.querySelector('.list')
while(true) {
    let input = prompt('please, enter an item');
    if(input === null) {break};
    list.insertAdjacentHTML('beforeend', `<li>${input}</li>`)
}

