'use strict';

/*
  Дан спан и кнопки +1, -1, которые будут увеличивать 
  и уменьшать на 1 значение спана. Сделайте так, чтобы 
  это значение не могло стать меньше нуля.
*/

const subBtn = document.querySelector('.js-sub');
const addBtn = document.querySelector('.js-add');
let value = document.querySelector('.js-value');

subBtn.addEventListener('click', someValue);
addBtn.addEventListener('click', someValue);

function someValue() {
  let sum = (parseInt(value.textContent) + parseInt(this.textContent));
  sum >= 0 ? value.textContent = `${sum}` : null;
}

