'use strict';

/*
  Напишите скрипт который реализует следующий функционал.
  
  Есть кнопка с классом button, текст которой отображает 
  кол-во раз которое по ней кликнули, обновляется при каждом клике.
*/

const btn = document.querySelector('.button');

function clickMe(obj) {
    let counter = 0;
    function inner() {
        obj.textContent = `${counter+=1}`;
    }
    return inner;
}

function addListener(obj) {
    obj.addEventListener('click', clickMe(obj));
}

addListener(btn);

// removeListener(btn);

// function removeListener(obj) {
//     obj.removeEventListener('click', clickMe(obj));
// }