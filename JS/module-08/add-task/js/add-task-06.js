'use strict';

/*
  Дан ul, а внутри него произвольное количество li с текстом и кнопкой. 
  Сделайте так, чтобы по нажатию на кнопку, удалялся тот li в котором
  она находится. Используйте делегирование.
*/


const list = document.querySelector('.list');

list.addEventListener('click', removeEl);

function removeEl({target, currentTarget}) {
    if(target.tagName === 'BUTTON' && target.textContent === 'Delete') {
        // currentTarget.removeChild(target.parentNode);
        target.parentNode.remove();
    }
}