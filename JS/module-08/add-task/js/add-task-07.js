'use strict';

/*
  Дан набор инпутов. Сделайте так, чтобы при потере фокуса все 
  инпуты проверяли свое содержимое на правильное количество символов. 
  
  Сколько символов должно быть в инпуте, указывается в атрибуте data-length. 
  Если введено подходящее количество, то outline инпута становится зеленым, 
  если неправильное - красным. 
*/

const inputs = document.querySelector('.inputs');

inputs.addEventListener('change', inputCheck);

function inputCheck({target}) {
    if(target.tagName !== 'INPUT') {return}
    if(parseInt(target.dataset.length) <= target.value.length){
        target.setAttribute('style', 'outline-width:1px; outline-color:rgb(0, 255, 0); outline-style: solid;');
    } else {
        target.setAttribute('style', 'outline-width:1px; outline-color:rgb(255, 0, 0); outline-style: solid;');
    }
}