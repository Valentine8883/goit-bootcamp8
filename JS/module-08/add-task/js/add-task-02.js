'use strict';

/*
  Даны 2 инпута, абзац и кнопка. По нажатию на кнопку 
  получите числа стоящие в инпутах и запишите их сумму в абзац.
*/

const addBtn = document.querySelector('.calc > button');
const args = document.querySelectorAll('.calc > input');
const result = document.querySelector('.result');

addBtn.addEventListener('click',  calcSum);

function calcSum() {
  result.textContent = `${parseInt(args[0].value) + parseInt(args[1].value)}`;
}

