'use strict';

/*
  Напишите скрипт который:
    
    - При фокусе текстового поля, в консоль выводит строку "Input is in focus!"
    - При наборе текста в текстовое поле (событие input), текущее его значение должно 
      отображаться в абзаце с классом input-value 
*/


const input = document.querySelector('.input');
const inputValue = document.querySelector('.input-value')

input.addEventListener('focus', inputOnFocus);
input.addEventListener('input', inputKayup);

function inputOnFocus() {
    console.log("Input is in focus!");
}

function inputKayup() {
    inputValue.textContent = `Current input value: ${input.value}`;
}