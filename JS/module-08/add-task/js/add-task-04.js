'use strict';
/*
  Есть форма с набором радиокнопок. Пользователь выбирает вариант ответа, 
  после чего нажимает кнопку "Send" и происходит отправка формы.
  
  При отправке формы:
    - не должна перезагружаться страница
    - необходимо получить выбранную опцию и вывести в абзац с классом .result
*/




const form = document.querySelector('.question-form');
const inputs = document.querySelectorAll('.question-form input')
let result = document.querySelector('.result')

console.log(inputs);

form.addEventListener('submit', submitAction);

function submitAction(event) {
    event.preventDefault();
    inputs.forEach(x => x.checked? result.textContent = 'Result: ' + x.value: null);
}