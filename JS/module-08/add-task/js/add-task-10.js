'use strict';

/*
  Ознакомьтесь с HTML и CSS.
  
  Есть меню навигации, необходимо написать скрипт, который
  при клике на пункт меню добавит ему класс menu-link-active,
  таким образом выделив текущую (активную) ссылку,
  при этом убрав его у всех остальных элементов меню.
  
  Пункотв меню может быть произвольное количество, используйте
  прием "Делегирование событий". Учтите клик по самому ul, его
  необходимо игнорировать.
  
  При клике по ссылкам не должна перезагружаться страница!
*/

const menu = document.querySelector('.js-menu');
const list= document.querySelectorAll('.js-menu a');

menu.addEventListener('click', menuOnClick);

function menuOnClick({target}) {
    event.preventDefault();
    if(target.className === 'menu-link') {
        list.forEach(x => x === target ? x.classList.add('menu-link-active'):x.classList.remove('menu-link-active')); 
    }
}