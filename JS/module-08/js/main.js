/*
  Создайте компонент галлереи изображений следующего вида.
  
    <div class="image-gallery js-image-gallery">
      <div class="fullview">
        <!-- Если выбран первый элемент из preview -->
        <img src="img/fullview-1.jpeg" alt="alt text 1">
      </div>
      <!-- li будет столько, сколько объектов в массиве картинок. Эти 3 для примера -->
      <ul class="preview">
        <li><img src="img/preview-1.jpeg" data-fullview="img/fullview-1.jpeg" alt="alt text 1"></li>
        <li><img src="img/preview-2.jpeg" data-fullview="img/fullview-2.jpeg" alt="alt text 2"></li>
        <li><img src="img/preview-3.jpeg" data-fullview="img/fullview-3.jpeg" alt="alt text 3"></li>
      </ul>
    </div>   
    
    🔔 Превью компонента: https://monosnap.com/file/5rVeRM8RYD6Wq2Nangp7E4TkroXZx2
      
      
    Реализуйте функционал:
      
      - image-gallery есть изначально в HTML-разметке как контейнер для компонента.
    
      - fullview содержит в себе увеличенную версию выбранного изображения из preview, и
        создается динамически при загрузке страницы.
    
      - preview это список маленьких изображений, обратите внимание на атрибут data-fullview,
        он содержит ссылку на большое изображение. preview и его элементы, также создаются 
        динамически, при загрузке страницы.
        
      - При клике в элемент preview, необходимо подменить src тега img внутри fullview
        на url из data-атрибута выбраного элемента.
        
      - По умолчанию, при загрузке страницы, активным должен быть первый элемент preview.
        
      - Изображений может быть произвольное количество.
      
      - Используйте делегирование для элементов preview.
      
      - При клике, выбраный элемент из preview должен получать произвольный эффект выделения.
      
      - CSS-оформление и имена классов на свой вкус.
      
      
    🔔 Изображения маленькие и большие можно взять с сервиса https://www.pexels.com/, выбрав при скачивании
      размер. Пусть маленькие изображения для preview будут 320px по ширине, большие для fullview 1280px.
      Подберите изображения одинаковых пропорций.
*/
  
/*
  ⚠️ ЗАДАНИЕ ПОВЫШЕННОЙ СЛОЖНОСТИ - ВЫПОЛНЯТЬ ПО ЖЕЛАНИЮ
  
  Создайте плагин галлереи используя ES6 класс. Добавьте поля и методы класса так, 
  чтобы можно было создать любое количество галлерей на странице. Функционал плагина 
  аналогичный заданию выше.
  
  При создании экземпляра конструктор получает:
    - items - список элементов для preview
    - parentNode - ссылку на DOM-узел в который будут помещены fullview и preview
    - defaultActiveItem - номер активного элемента preview по умолчанию
    
  Тогда создание экземпляра будет выглядеть следующим образом.
*/

//   new Gallery({
//     items: galleryItems,
//     parentNode: document.querySelector('.image-gallery'),
//     defaultActiveItem: 1
//   });

/* Далее плагин работает в автономном режиме */


/*
  Массив объектов с данными для создания компонента выглядит следующим образом.
  Замените пути на соотвествующие вашим, или назовите изображения аналогично.
*/

const galleryItems1 = [
    { preview: './img/preview-1-1.jpg', fullview: './img/fullview-1-1.jpg', alt: "image 1" },
    { preview: './img/preview-1-2.jpeg', fullview: './img/fullview-1-2.jpeg', alt: "image 2" },
    { preview: './img/preview-1-3.jpeg', fullview: './img/fullview-1-3.jpeg', alt: "image 3" },
    { preview: './img/preview-1-4.jpeg', fullview: './img/fullview-1-4.jpeg', alt: "image 4" },
    { preview: './img/preview-1-5.jpeg', fullview: './img/fullview-1-5.jpeg', alt: "image 5" },
    { preview: './img/preview-1-6.jpeg', fullview: './img/fullview-1-6.jpeg', alt: "image 6" },
    { preview: './img/preview-1-7.jpeg', fullview: './img/fullview-1-7.jpg', alt: "image 7" },
  ];
  
  const galleryItems2 = [
    { preview: './img/preview-2-1.jpg', fullview: './img/fullview-2-1.jpg', alt: "image 1" },
    { preview: './img/preview-2-2.jpg', fullview: './img/fullview-2-2.jpg', alt: "image 2" },
    { preview: './img/preview-2-3.jpg', fullview: './img/fullview-2-3.jpg', alt: "image 3" },
    { preview: './img/preview-2-4.jpg', fullview: './img/fullview-2-4.jpg', alt: "image 4" },
    { preview: './img/preview-2-5.jpg', fullview: './img/fullview-2-5.jpg', alt: "image 5" },
    { preview: './img/preview-2-6.jpg', fullview: './img/fullview-2-6.jpg', alt: "image 6" },
  ];
  
  class  galleryPlugin {
    constructor(items, parentNode, defaultActiveItem ) {
      this.items = items;
      this.parentNode = document.querySelector(parentNode);
      this.defaultActiveItem = defaultActiveItem ;
    }
  
    addImages(images, galleryNumber = 1) { 
      for(let i = 0; i < images.length; i+=1) {
        let previewItem = document.createElement('li');
        let previewImg = document.createElement('img');
        previewImg.src = images[i].preview;
        previewImg.alt = images[i].alt;
        previewImg.dataset.fullview = images[i].fullview;
        previewItem.append(previewImg);
        document.querySelectorAll('.preview')[galleryNumber - 1].append(previewItem);
      }
    }
  
    createGallery() {
      const fullView = document.createElement('div');
      fullView.classList.add('fullview');
      const fullViewImg = document.createElement('img');
      fullViewImg.src = this.items[this.defaultActiveItem - 1].fullview;
      this.parentNode.append(fullView);
      fullView.append(fullViewImg);
      const previewList = document.createElement('ul');
      previewList.classList.add('preview');
      previewList.addEventListener('click', function({target}) {
        target.tagName === 'IMG'? fullViewImg.src = target.dataset.fullview : null;
        document.querySelectorAll('.preview img').forEach(x => x === target ? x.classList.add('imageShadow'):x.classList.remove('imageShadow')); 
      });
      this.parentNode.append(previewList);
      this.addImages(this.items, document.querySelectorAll('.preview').length);
    }
  }
  
  new galleryPlugin(galleryItems2, '.js-image-gallery', 1).createGallery();
  new galleryPlugin(galleryItems1, '.js-image-gallery', 1).createGallery();
  new galleryPlugin(galleryItems2, '.js-image-gallery', 1).createGallery();
