'use strict';

/*
  ⚠️ ДОПОЛНИТЕЛЬНОЕ ЗАДАНИЕ - ВЫПОЛНЯТЬ ПО ЖЕЛАНИЮ
  
  Создайте скрипт турагенства, продающего поездки в 3-х группах: sharm, hurgada и taba.
  Кол-во мест в группах ограничено (создайте переменные для хранения мест в группах): 
    * sharm - 15
    * hurgada - 25
    * taba - 6.
  Когда пользователь посещает страницу, ему необходимо предложить ввести число необходимых мест,
  результат сохранить в переменную.
  Необходимо проверить являются ли введенные данные целым положительным числом. 
  
    - В случае неверного ввода от пользователя, скрипт показывает alert с текстом 
      "Ошибка ввода" и больше ничего не делает.
    - В случае верного ввода, последовательно проверить кол-во мест в группах, 
      и кол-во необходимых мест введенных пользователем.
  Если была найдена группа в которой количество мест больше либо равно необходимому, 
  вывести сообщение через confirm, что есть место в группе такой-то, согласен ли 
  пользоваетель быть в этой группе?
    * Если ответ да, показать alert с текстом 'Приятного путешествия в группе <имя группы>'
    * Если ответ нет, показать alert с текстом 'Нам очень жаль, приходите еще!'
  
  Если мест нигде нет, показать alert с сообщением 'Извините, столько мест нет ни в одной группе!'
*/

let sharm = 15;
let hurgada = 25;
let taba = 6;

const inputErrorMessage = 'Ошибка ввода';
const niceTripMessage = 'Приятного путешествия в группе ';
const sorryMessage = 'Нам очень жаль, приходите еще!';
const notEnoughPlacesMessage = 'Извините, столько мест нет ни в одной группе!';

let inputNumber = prompt('Please, enter the number of seats');
let confirmFlag;

if (!Number.isNaN(inputNumber) && inputNumber % 1 == 0) {
    if(inputNumber <= taba) {
        confirmFlag = confirm('Есть место в группе "taba", согласны ли вы быть в этой группе?');
        alert(confirmFlag === true ? niceTripMessage + 'taba' : sorryMessage);
    } else if (inputNumber <= sharm) {
        confirmFlag = confirm('Есть место в группе "sharm", согласны ли вы быть в этой группе?');
        alert(confirmFlag === true ? niceTripMessage + 'sharm' : sorryMessage);
    } else if (inputNumber <= hurgada) {
        confirmFlag = confirm('Есть место в группе "hurgada", согласны ли вы быть в этой группе?');
        alert(confirmFlag === true ? niceTripMessage + 'hurgada' : sorryMessage);
    } else {
        alert(notEnoughPlacesMessage);
    }
} else {
    alert(inputErrorMessage);
}