/*
  Дан массив цветов и кнопки "Start" и "Stop". Сделайте так, чтобы после
  нажатия кнопки "Start", каждую секунду body менял цвет фона на случайное 
  значение из массива. 

  При нажатии на кнопку "Stop", изменении цвета фона должно останавливаться.
*/

const colors = ['#FFFFFF', '#F44336', '#2196F3', '#4CAF50', '#FF9800', '#009688', '#795548'];

let start = document.querySelector('.js-start');
let stop = document.querySelector('.js-stop');

let intervalValue;

function randomBackground() {
    document.body.style.backgroundColor = colors[parseInt(Math.random() * 7)]; 
}

function interval() {
    intervalValue = setInterval(randomBackground, 100);
}

function stopInterval() {
    clearInterval(intervalValue)
}

start.addEventListener('click', interval);
stop.addEventListener('click', stopInterval);

console.log(intervalValue)

