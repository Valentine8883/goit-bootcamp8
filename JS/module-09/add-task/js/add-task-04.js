/*
  Напишите скрипт работы магазина со складом товаров.
  
  Есть переменная goodsAmount хранящиая в себе
  текущее количество единиц какого-то товара на складе.
  
  Напишите функцию processOrder(amount), получающую
  кол-во товаров заказанных покупателем, и возвращающую промис.
  
  Для имитации проверки достаточного количества товаров
  на складе используйте setTimeout с delay 500мс.
  
  Если на складе товаров больше либо равно заказанному
  количеству, "верните" строку - "Ваш заказ готов!".
  
  В противном случае - "К сожалению на складе не достаточно товаров!".
  
  Если же пользователь ввел не число, то выдайте ошибку throw new Error("Некорректный ввод!")  
*/

const DELAY = 5000;

let goodsAmount = 100;

// function processOrder(amount) {
//   return new Promise((resolve, reject) => {
//     setTimeout(_ => {
//       if(isNaN(amount)) {
//         reject('Некорректный ввод!');
//       } else {
//         goodsAmount - amount > 0 ? resolve("Ваш заказ готов!") : resolve("К сожалению на складе не достаточно товаров!");
//       }
//     }, DELAY);
//   });
// }

function processOrder(amount) {
  return Promise.resolve(!isNaN(amount) ? (goodsAmount - amount > 0 ? "Ваш заказ готов!" : "К сожалению на складе не достаточно товаров!") : 'Некорректный ввод!');
}

// Вызовы функции для проверки
processOrder(50)
  .then(x => console.log(x)) // Ваш заказ готов!
  .catch(err => console.log(err));

processOrder(50)
  .then(x => console.log(x)) // Ваш заказ готов!
  .catch(err => console.log(err));

processOrder(500)
  .then(x => console.log(x)) // К сожалению на складе недостаточно товаров!
  .catch(err => console.log(err));

processOrder("qwe")
  .then(x => console.log(x))
  .catch(err => console.log(err)); // Некоректный ввод!
/*******************************************************************************/
// const promise = new Promise((onResolve, onReject) => {
//   setTimeout(() => {
//     onResolve(5);
//     // onReject('promise rejected');
//   }, 1000);
// });

// promise
// .then(x => {
//   console.log(x);
//   return x * 4;
// }).then(x => console.log(x));
/*******************************************************************************/

// const checkNumber = num => {
//   return new Promise((resolve, reject) => {
//     setTimeout(() => {
//       if (num % 2 === 0) {
//         resolve('even!');
//       } else {
//         reject('add! error');
//       }

//     }, 1000);

//   })
// }


// checkNumber(3).then(x=> console.log(x)).catch(e => console.log(e));
/*******************************************************************************/

// const DISTANCE = 1000;

// const race = (name, speed) => new Promise((resolve) => {
//   setTimeout(() => {
//     resolve(`${name} crossed finish line`);

//   }, (DISTANCE / speed) * 1000);
// })


// const mango = race('mango', 300);
// const poly = race('poly', 500);
// const ajax = race('ajax', 700);


// Promise.all([mango, poly, ajax]).then(arr => console.log(arr));
// Promise.race([mango, poly, ajax]).then(x => console.log(x));