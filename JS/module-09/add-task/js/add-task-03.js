/* 
  Напишите скрипт, реализующий базовый функционал
  таймера, запуск отсчета времени и сброс счетчика
  в исходное состояние.
  
  Создайте функцию startTimer, которая будет запускать
  отсчет времени с момента ее нажатия, она вызывается 
  при клике на кнопку с классом js-timer-start.
  
  Создайте функцию stopTimer, которая будет останавливать
  счетчик, она вызывается при клике на кнопку с классом js-timer-stop.
  
  Используйте вспомогательную функцию updateClockface 
  которая обновляет значение счетчика в интерфейсе. 
  Для составления строки времени в формате xx:xx.x, 
  исользуйте функцию getFormattedTime из задания номер 1.
  
  Подсказка: так как нам интересны исключительно сотни миллисекунд,
      нет смысла выполнять пересчет времени чаще чем каждые 100мс.
*/

const clockface = document.querySelector(".js-clockface");
const startBtn = document.querySelector(".js-timer-start");
const stopBtn = document.querySelector(".js-timer-stop");

let interval;

// let startDate = new Date();

const timer = {
  startTime: new Date(),
  deltaTime: null,
  id: null
};



function setTimer() {
    let today = Date.now();
    let delta = today - timer.startTime;
    let minutes = Math.floor(delta / 1000 / 60 % 60);
    let seconds = Math.floor(delta / 1000 % 60);
    let ms = Math.floor(delta % 1000 / 100 );
    clockface.textContent = `${minutes < 10 ? `0${minutes}`: minutes}:${seconds < 10 ? `0${seconds}`: seconds}.${ms}`;

}

function startInterval() {
    interval =  setInterval(setTimer, 100);
}

startBtn.addEventListener('click', startInterval);

function clear() {
    clearInterval(interval);
}

stopBtn.addEventListener('click', clear);


/*
* Вспомогательные функции
*/

/*
* Обновляет поле счетчика новым значением при вызове
* аргумент time это кол-во миллисекунд
// */
// function updateClockface(elem, time) {
//   // Используйте функцию getFormattedTime из задания #1
//   // elem.textContent = getFormattedTime(time);
// }

// /*
// * Подсветка активной кнопки
// */
// function setActiveBtn(target) {
//   if(target.classList.contains('active')) {
//     return;
//   }
  
//   startBtn.classList.remove('active');
//   stopBtn.classList.remove('active');
  
//   target.classList.add('active');
// }

