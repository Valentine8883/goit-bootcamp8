'use strict';

/*
  Создайте скрипт секундомера.  
  По ссылке можно посмотреть пример выбрав Stopwatch http://www.online-stopwatch.com/full-screen-stopwatch/
  
  Изначально в HTML есть разметка:
  
  <div class="stopwatch">
    <p class="time js-time">00:00.0</p>
    <button class="btn js-start">Start</button>
    <button class="btn js-take-lap">Lap</button>
    <button class="btn js-reset">Reset</button>
  </div>
  <ul class="laps js-laps"></ul>
  
  Добавьте следующий функционал:
  
  - При нажатии на кнопку button.js-start, запускается таймер, который считает время 
    со старта и до текущего момента времени, обновляя содержимое элемента p.js-time 
    новым значение времени в формате xx:xx.x (минуты:секунды.сотни_миллисекунд).
       
    🔔 Подсказка: так как необходимо отображать только сотни миллисекунд, интервал
                  достаточно повторять не чаще чем 1 раз в 100 мс.
    
  - Когда секундомер запущен, текст кнопки button.js-start меняется на 'Pause', 
    а функционал при клике превращается в оставновку секундомера без сброса 
    значений времени.
    
    🔔 Подсказка: вам понадобится буль который описывает состояние таймера активен/неактивен.
  
  - Если секундомер находится в состоянии паузы, текст на кнопке button.js-start
    меняется на 'Continue'. При следующем клике в нее, продолжается отсчет времени, 
    а текст меняется на 'Pause'. То есть если во время нажатия 'Pause' прошло 6 секунд 
    со старта, при нажатии 'Continue' 10 секунд спустя, секундомер продолжит отсчет времени 
    с 6 секунд, а не с 16. 
    
    🔔 Подсказка: сохраните время секундомера на момент паузы и используйте его 
                  при рассчете текущего времени после возобновления таймера отнимая
                  это значение от времени запуска таймера.
    
  - Если секундомер находится в активном состоянии или в состоянии паузы, кнопка 
    button.js-reset должна быть активна (на нее можно кликнуть), в противном случае
    disabled. Функционал при клике - остановка таймера и сброс всех полей в исходное состояние.
    
  - Функционал кнопки button.js-take-lap при клике - сохранение текущего времени секундомера 
    в массив и добавление в ul.js-laps нового li с сохраненным временем в формате xx:xx.x
*/

/*
  ⚠️ ЗАДАНИЕ ПОВЫШЕННОЙ СЛОЖНОСТИ - ВЫПОЛНЯТЬ ПО ЖЕЛАНИЮ
  
  Выполните домашнее задание используя класс с полями и методами.
  
  На вход класс Stopwatch принимает только ссылку на DOM-узел в котором будет 
  динамически создана вся разметка для секундомера.
  
  Должна быть возможность создать сколько угодно экземпляров секундоментов 
  на странице и все они будут работать независимо.
  
  К примеру:
  
  new Stopwatch(parentA);
  new Stopwatch(parentB);
  new Stopwatch(parentC);
  
  Где parent* это существующий DOM-узел. 
*/

class Timer {
  constructor(parentNode) {
    this.startTime = null;
    this.deltaTime = null;
    this.timerId = null;
    this.parentNode = document.querySelector(parentNode);
    this.rootDiv = document.createElement('div');
    this.clockface = document.createElement('p')
    this.startBtn = document.createElement('button');
    this.takeLapBtn = document.createElement('button');
    this.resetBtn = document.createElement('button');
    this.lapList = document.createElement('ul');
    this.createTimerUI();
  }

  createTimerUI() {
    this.clockface.classList.add('time', 'js-time');
    this.clockface.textContent = '00:00.0';
    this.startBtn.classList.add('btn', 'js-start');
    this.startBtn.textContent = 'Start';
    this.takeLapBtn.classList.add('btn', 'js-take-lap');
    this.takeLapBtn.textContent = 'Lap';
    this.takeLapBtn.disabled = true;
    this.resetBtn.classList.add('btn', 'js-reset');
    this.resetBtn.textContent = 'Reset';
    this.lapList.classList.add('laps', 'js-laps');
    this.rootDiv.classList.add('js-root')
    this.rootDiv.append(this.clockface, this.startBtn, this.takeLapBtn, this.resetBtn, this.lapList);
    this.parentNode.append(this.rootDiv);
    this.rootDiv.addEventListener('click', this.handleBtn.bind(this));
  }
  updateClockface(min, sec, ms) {
    this.clockface.textContent = `${min<10 ? '0'+min: min}:${sec < 10 ? '0'+sec : sec}.${ms}`
  }

  handleBtn({target}) {
    switch(target.textContent) {
      case('Start'):
        this.start();
        break;
      case('Lap'):
        this.lap();
        break;
      case('Reset'):
        this.reset();
        break;
      case('Pause'):
        this.pause();
        break;
      case('Continue'):
        this.start();
        break;
    }
  }

  start() {
    this.takeLapBtn.disabled = false;
    this.startBtn.textContent = 'Pause';
    this.startTime = Date.now() - this.deltaTime;
    this.timerId = setInterval(() => {
      let currentTime = Date.now();
      this.deltaTime = currentTime - this.startTime;
      let ms =  Math.floor(this.deltaTime % 1000 / 100);
      let sec= Math.floor(this.deltaTime / 1000 % 60);
      let min = Math.floor(this.deltaTime / 1000 / 60 % 60);
      this.updateClockface(min, sec, ms);
    }, 100);
  }

  reset() {
    clearInterval(this.timerId);
    this.timerId = null;
    this.deltaTime = null;
    this.takeLapBtn.disabled = true;
    this.startBtn.textContent = 'Start';
    this.lapList.innerHTML = '';
    this.updateClockface(0, 0, 0);
  }

  pause() {
    clearInterval(this.timerId);
    this.timerId = null;
    this.startBtn.textContent = 'Continue';
    this.takeLapBtn.disabled = true;
  }

  lap() {
    const item = document.createElement('li');
    item.textContent = this.clockface.textContent;
    this.lapList.appendChild(item);
  }
}

new Timer('body');
new Timer('body');

