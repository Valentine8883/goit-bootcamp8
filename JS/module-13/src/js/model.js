import v4 from 'uuid/v4';
import * as storage from '../services/storage';
import {getSiteImg} from '../services/api';

export default class Model {
    constructor() {
        this.items = [];
        storage.get() ? this.items = storage.get() : null;
    }

    addItem(url) {
        return getSiteImg(url).then(data => {
            const item = {id: v4(), url, imgSrc:'', descr: ''};
            data.description ? item.descr = data.description : item.descr = 'No description';
            data.image ? item.imgSrc = data.image : item.imgSrc = 'https://alternatetutelage.files.wordpress.com/2017/09/image.png?w=640';
            this.items.push(item);
            storage.set(this.items);
            return item;
        });
    }

    removeItem(id) {
        this.items = this.items.filter(x => x.id !== id);
        storage.remove;
        storage.set(this.items);
    }
 }