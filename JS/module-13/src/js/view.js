import EventEmitter from '../services/event-emitter';

export default class View extends EventEmitter{
    constructor() {
        super();
        this.form = document.querySelector('.form');
        this.input = this.form.querySelector('.input');
        this.rootList = document.querySelector('.root-list');
        this.form.addEventListener('submit', this.handleAdd.bind(this));
        this.urlPattern = /^((https||http)?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/i;
    }

    createSite(site) {
        const item = document.createElement('li');
        item.classList.add('item');
        item.dataset.id = site.id;

        const url = document.createElement('p');
        url.classList.add('url');
        url.textContent = site.url;

        const buttonRemove = document.createElement('button');
        buttonRemove .classList.add('button');
        buttonRemove .dataset.action = 'remove'
        buttonRemove .textContent = 'Remove';

        const img = document.createElement('img');
        img.alt = 'site image';
        img.src = site.imgSrc;
        img.classList.add('js-site-img');

        const description = document.createElement('p');
        description.textContent = site.descr;
        description.classList.add('site-description');

        item.append(url, img, description, buttonRemove);

        this.appendEventListeners(item);
        return item;
    }

    addSite(site) {
        const item = this.createSite(site);
        this.form.reset();
        this.rootList.appendChild(item);
    }

    appendEventListeners(item) {
        const removeBtn = item.querySelector('[data-action="remove"]');
        removeBtn.addEventListener('click', this.handleRemove.bind(this));
    }

    handleRemove({target}) {
        const parent = target.closest('.item');
        this.emit('remove', parent.dataset.id);
    }

    handleAdd(evt) {
        evt.preventDefault();
        const {value} = this.input;
        if(value === '') {
            alert('String cannot be empty');
            return;
        }
        if(!this.urlPattern.test(value)) {
            alert('URL is not url');
            return;
        }

        const items = this.rootList.querySelectorAll('.url');
        (Array.from(items).find(x => x.textContent.toLowerCase() === value.toLowerCase())) !== undefined ? alert('url is not unique'): this.emit('add', value.toLowerCase());
    }

    removeSite(id) {
        const items = this.rootList.querySelectorAll('.item');
        items.forEach(x => x.dataset.id === id ? x.remove() : null);
    }
}