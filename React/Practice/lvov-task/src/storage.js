const storageName = 'contact-holder';

export function set(value) {
    localStorage.setItem(storageName, JSON.stringify(value));
}

export function get() {
    const data = localStorage.getItem(storageName);
    return data ? JSON.parse(data) : null;
}

export function remove() {
    localStorage.removeItem(storageName);
}



