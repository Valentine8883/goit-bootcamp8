import React from 'react';
import './Modal.css';
import Button from '../Button/Button.jsx'

const Modal = ({toggle, name, lastName, phone, email, createObj, handleChange, id, edit}) => {
    let submit = () => {
        toggle();
        createObj();
    }
    return (
        <div className="owerlay" >
            <div className='modal'>
                <form className="form" >
                    <input type="text" value={name} onChange={handleChange} name="name" placeholder="first name"/>
                    <input type="text" value={lastName} onChange={handleChange} name="lastName" placeholder="last name"/>
                    <input type="text" value={phone} onChange={handleChange} name="phone" placeholder="phone number"/>
                    <input type="text" value={email} onChange={handleChange} name="email" placeholder="email"/>
                    <Button text={edit? 'Eddit contact' : 'Create contact'} onclick={submit}/>
                </form>
            </div>
        </div>
    );
};

export default Modal;