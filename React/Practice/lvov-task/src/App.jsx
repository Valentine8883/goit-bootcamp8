import React, { Component } from 'react';
import './App.css';
import Card from './Card/Card.jsx'
import Button from './Button/Button.jsx'
import Modal from './Modal/Modal.jsx'
import id4 from 'uuid';
import * as storage from './storage.js'

class App extends Component {
  constructor() {
    super();
    this.state = {
      cards: storage.get() || [],
      name: '',
      lastName: '',
      phone: '',
      email: '',
      id: '',
      showModal: false,
      edit: false,
    }
  }

toggleModal =() => {
  this.setState(prev => ({ showModal: !prev.showModal }))
}

createObj = () => {
  if(this.state.edit) {
    this.setState({
      cards: this.state.cards.map(x=> x.id === this.state.id ? {...x, name:this.state.name,lastName:this.state.lastName, phone:this.state.phone, email:this.state.email} : x),
      edit:false,
      name: '', lastName:'', phone:'', email:'', id:''
    });
  } else {
    let cards = {id: id4(), name: this.state.name, lastName:this.state.lastName, phone:this.state.phone, email:this.state.email};
    this.setState(prevState => ({
    cards: [...prevState.cards, cards],
    name: '', lastName:'', phone:'', email:'', id:'',
  }));
  }
}

handleChange =({target}) => {
  this.setState ({
    [target.name]: target.value,
  })
}

removeItem = ({target}) => {
  let id = target.parentNode.dataset.id;
  this.setState({
    cards: this.state.cards.filter(x => x.id !== id)
  });
}

edit = ({target}) => {
  let id = target.parentNode.dataset.id;
  let result = this.state.cards.find(x => x.id === id);
  this.setState({
    name: result.name,
    lastName: result.lastName,
    phone: result.phone,
    email: result.email,
    id: result.id,
    showModal: true,
    edit: true
  })
}

render() {
  let {showModal, cards, name, lastName, phone, email, id, edit} = this.state
  return (
    <div className="contact-book">
      {showModal && <Modal name={name} lastName = {lastName} phone={phone} email={email}toggle={this.toggleModal} createObj={this.createObj} handleChange={this.handleChange} editObj={this.editObj} id={id} edit={edit}/>}
       <Button text="Create contact" onclick={this.toggleModal}/>
      <ul className='list'>
        {cards.map(x => <Card name={x.name} lName={x.lastName} phone={x.phone} email={x.email} id={x.id} remove={this.removeItem} edit={this.edit}/>)}
      </ul>
    </div>
  );
}
}

export default App;
