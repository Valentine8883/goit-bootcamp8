import React from 'react';
import './Card.css';
import PropTypes from 'prop-types';



const Card = ({name, lName, phone, email, id, remove, edit}) => {
    return (
        <li key={id} data-id={id} className={'list-item'}> 
            <p className='name'>Name :{name}_{lName}</p>
            <p className='telephone'>Phone :{phone}</p>
            <p className='mail'>Email :{email}</p>
            <button onClick={remove}>remove</button>
            <button onClick={edit}>edit</button>
        </li>
    );
};


Card.propTypes = {
    name: PropTypes.string.isRequired,
    lName: PropTypes.string.isRequired,
    phone: PropTypes.string,
    email: PropTypes.string,
    id:PropTypes.string,
    remove: PropTypes.func
}

export default Card;