import React from 'react';
import './Main.css';
import UserInfo from '../UserInfo/UserInfo';
import News from '../News/News.jsx'

const Main = () => {
    return (
        <main className="main">
        <UserInfo/>
        <News/>
        </main>
    );
};

export default Main;