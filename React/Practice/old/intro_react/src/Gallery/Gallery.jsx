import React from 'react';
import './Gallery.css';

const Gallery = () => {
    let imgArr = [ 
    {src: "https://is2-ssl.mzstatic.com/image/thumb/Music128/v4/b9/3d/34/b93d3404-3aa6-c13d-48ca-12106d98b462/source/1200x630bb.jpg", id: 1},
    {src: "https://is2-ssl.mzstatic.com/image/thumb/Music128/v4/b9/3d/34/b93d3404-3aa6-c13d-48ca-12106d98b462/source/1200x630bb.jpg", id: 2},
    {src: "https://is2-ssl.mzstatic.com/image/thumb/Music128/v4/b9/3d/34/b93d3404-3aa6-c13d-48ca-12106d98b462/source/1200x630bb.jpg", id: 3},
    {src: "https://is2-ssl.mzstatic.com/image/thumb/Music128/v4/b9/3d/34/b93d3404-3aa6-c13d-48ca-12106d98b462/source/1200x630bb.jpg", id: 4},
    {src: "https://is2-ssl.mzstatic.com/image/thumb/Music128/v4/b9/3d/34/b93d3404-3aa6-c13d-48ca-12106d98b462/source/1200x630bb.jpg", id: 5},
    {src: "https://is2-ssl.mzstatic.com/image/thumb/Music128/v4/b9/3d/34/b93d3404-3aa6-c13d-48ca-12106d98b462/source/1200x630bb.jpg", id: 6}];
    return (
        <div className="gallery">
            {imgArr.map(x=> <img className="gallety-item" src={x.src} alt="" key={x.id}/>)}
   
        </div>
    );
};
export default Gallery;