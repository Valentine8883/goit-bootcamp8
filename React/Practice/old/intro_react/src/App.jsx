import React from 'react';
import "./App.css";
import Header from './Header/Header.jsx'
import Main from './Main/Main.jsx';

const App = () => {
    return (
        <div>
            <Header/>
            <Main/>
        </div>
    );
};

export default App;


