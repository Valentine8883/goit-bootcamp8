import React from 'react';
import './UserInfo.css';
import Gallery from '../Gallery/Gallery.jsx'

const UserInfo = () => {
    return (
        <div className="info">
            <img src="https://is4-ssl.mzstatic.com/image/thumb/Music128/v4/61/38/7d/61387dfa-0594-0fdb-60ee-52e24c584cb1/source/1200x630bb.jpg" alt="" className="photo"/>
            <h2 className="name">Valentine Filippov</h2>
            <h3 className="nickname">8883</h3>
            <Gallery/>

        </div>
    );
};

export default UserInfo;