import React from 'react';
import './Item.css';
import PropTypes from 'prop-types';

const Item = ({title, logo, item, desc, bgt, productDesc, productDescSecond, bage, caption, sizes, colors, price, retail}, increment) => {
    return (
        <div className="container" key={title}>
        <div className="card">
          <div className="card-head">
            <img src={logo} alt="logo" className="card-logo"/>
            <img src={item} alt="Shoe" className="product-img"/>
            <div className="product-detail">
              <h2>{title}</h2>{desc}
            </div>
            <span className="back-text">{bgt}</span>
          </div>
          <div className="card-body">
            <div className="product-desc">
              <span className="product-title">
                      {productDesc}<b>{productDescSecond}</b>
                      <span className="badge">{bage}</span>
              </span>
              <span className="product-caption">{caption}</span>
            </div>
            <div className="product-properties">
              <span className="product-size">
                      <h4>Size</h4>
                      <ul className="ul-size">
                      {sizes.map(x => <li key={x.title+x}><a href="#">{x}</a></li>)}
                      </ul>
              </span>
              <span className="product-color">
                      <h4>Colour</h4>
                      <ul className="ul-color">{colors.map(x=> <li key={x.title+x}><a href="#" className={x}></a></li>)}</ul>
              </span>
              <span className="product-price">USD<b>{price}</b></span>
            </div>
            <button className="add" data-price={price} data-title={title} onClick={increment}>Add to cart</button>
          </div>
        </div>
      </div>
    );
};

Item.prototype = {
  title: PropTypes.string.isRequired,
  logo: PropTypes.string.isRequired,
  item: PropTypes.string.isRequired,
  desc: PropTypes.string.isRequired,
  bgt: PropTypes.string,
  productDesc: PropTypes.string, 
  productDescSecond: PropTypes.string,
  bage: PropTypes.string.isRequired,
  caption: PropTypes.string.isRequired, 
  sizes: PropTypes.arrayOf(PropTypes.number).isRequired, 
  colors: PropTypes.arrayOf(PropTypes.string).isRequired,
  price: PropTypes.string.isRequired,
  retail: PropTypes.shape({
    buy: PropTypes.number,
    sell: PropTypes.number,
  }),
  increment: PropTypes.func.isRequired,
};

Item.defaultProps = {
  title: 'some title',

}


export default Item;