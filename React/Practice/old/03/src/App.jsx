import React, { Component } from 'react';
import './App.css';
import './Item/Item.jsx'
import Item from './Item/Item.jsx';

export default class App extends Component {

  state = {shoes: [{
    logo: 'https://s5.postimg.cc/wy79025cz/nike_Logo_White.png',
    item: 'https://s5.postimg.cc/j9r8yf9gn/sws1.png',
    title: 'Hartbeespoort',
    desc: 'Support and Nike Zoom Air come together for a more supportive feel with high-speed responsiveness',
    bgt: 'FAS',
    productDesc: 'Hartbee',
    productDescSecond: 'spoort',
    bage: 'New',
    caption: 'Basket Ball Collection',
    sizes: [7,8,9,10,11],
    colors: ['orange', 'green', 'yellow'],
    price: '23.45',
},
{
    logo: 'https://s5.postimg.cc/wy79025cz/nike_Logo_White.png',
    item: 'https://s5.postimg.cc/j9r8yf9gn/sws1.png',
    title: 'Nike Air Jordan ',
    desc: 'Nike Air Jordan I 1 Retro Mens Shoes Leather Black Blue',
    bgt: 'NAJ',
    productDesc: 'Nike',
    productDescSecond: 'Air Jordan',
    bage: 'New',
    caption: "Men's Basketball",
    sizes: [6,7,8,12],
    colors: ['green', 'yellow'],
    price: '88.19',
},{
    logo: 'https://s5.postimg.cc/wy79025cz/nike_Logo_White.png',
    item: 'https://s5.postimg.cc/j9r8yf9gn/sws1.png',
    title: 'Nike Air Huarache',
    desc: 'Nike Air Huarache Run Ultra BR Running Shoes Sneakers Dark Grey Menta Black',
    bgt: 'NAH',
    productDesc: 'Nike',
    productDescSecond: 'Air Huarache',
    bage: 'sale',
    caption: 'Unisex Running',
    sizes: [10,11,12,13],
    colors: ['orange'],
    price: '78.21',
    retail: {
      buy: 55.01,
      sell: 78,
    }
}], cartCounter: 0, totalPrice: 0, cart: []}

increment = (evt) => {
  let title = evt.target.dataset.title;
  let result = this.state.shoes.find(x=> x.title === title);
  this.setState(prev => ({
    cartCounter: prev.cartCounter+1,
    // totalPrice: prev.totalPrice + price,
    cart: [...prev.cart, result],

  })
)
}

render() {
  const {shoes, cartCounter, totalPrice, cart} = this.state
  let sum = cart.reduce((acc, x) => acc + Number(x.price), 0);
  return (
    <div className="box">
    {/* <div>
      <p className="counter">Count: {cartCounter}</p>
      <p className="counter">Total sum: {totalPrice.toFixed(2)}</p>
    </div> */}
    <div className="cart">
    <p className="counter">Count: {cartCounter}</p>
    {cart.map(x => <p>{x.title}:{x.price}</p>)}
    <p>{sum.toFixed(2)}$</p>

    </div>
    
    {shoes.map(x => Item(x, this.increment))}
    </div>
  )
}
}
