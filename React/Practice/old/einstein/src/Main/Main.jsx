import React from 'react';
import './Main.css';
import Description from '../Description/Description.jsx'
import Timeline from '../Timeline/Timeline.jsx'

const Main = () => {
    return (
        <main id="main">
            <Description/>
            <Timeline/> 
        </main>
    );
};

export default Main;