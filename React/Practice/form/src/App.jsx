import React, { Component } from 'react';
import './App.css';
import id4 from 'uuid';
import romoveIcon from './trash.png';
import Modal from './Modal/Madal.jsx';
class App extends Component {

  state ={
    text:'',
    password:'',
    tasks:[],
    showModal: false,
  }

handleChange =({target}) => {
  let value = target.value;
  let name = target.name;
  this.setState ({
    [name]: value,
  })
}

createObj = (evt) => {
  evt.preventDefault();
  let item = {id: id4(), text: this.state.text, complete: false}
  this.setState(prevState => ({
    tasks: [...prevState.tasks, item],
    text: ''
  }))
}

toggleComplete = ({target}) => {
  let id = target.id;
  this.setState({
    tasks: this.state.tasks.map(x => x.id === id ? {...x, complete: !x.complete} : x)

  })
}

removeItem = ({target}) => {
  let id = target.dataset.id;
  this.setState({
    tasks: this.state.tasks.filter(x => x.id !== id)
  })
}

toggleModal =() => {
  this.setState(prev => ({ showModal: !prev.showModal }))

}

render() {
  let {text, tasks, showModal} = this.state;
  return (
    <div className="App">
    {showModal && <Modal toggle={this.toggleModal}/>}
     <button onClick={this.toggleModal}>Show modal window</button>
      <form className="form" onSubmit={this.createObj}>
        <input type="text" className="text" value={text} onChange={this.handleChange} name="text" placeholder="name"/>
        <input type="submit" className="add" value="Add"/>
      </form>
      <ul className="list">{tasks.map(x => <li className={x.complete? "list-item done" : "list-item"} onClickCapture={this.toggleComplete} id={x.id} key={x.id}>
      <img data-id={x.id} alt="some icon" src={romoveIcon} onClick={this.removeItem}/>{x.text} </li>)}</ul>
    </div>
  );
}
}

export default App;