import React from 'react';
import style from './Prise.css'

const Price = ({price}) => {
    return (
        <p className={style.price}>Price: {price}₴</p>
    );
};

export default Price;