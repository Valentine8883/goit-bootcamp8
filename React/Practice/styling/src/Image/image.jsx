import React from 'react';
import style from './Image.css';


const image = ({image}) => {
    return (
        <img className={style.image} alt='house' src={image}/>  
    );
};

export default image;