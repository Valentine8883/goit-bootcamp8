import React from 'react';
import style from './Area.css';

const Area = ({area}) => {
    return (
        <p className={style.area}>AREA: {area}</p>
    );
};

export default Area;