import React, { Component } from 'react';
import './App.css';
import style from './App.css';
import Item from './Card/Card.jsx';
import Button from './Button/Button.jsx'
import axios from 'axios';


class App extends Component {
  state = {
    data: [],
    template: [],
    dataIsLoading: false,
    templateIsLoading: false,
    error: null,
  }

  componentDidMount() {
      this.setState({dataIsLoading: true, templateIsLoading: true});

      axios.get('http://demo4452328.mockable.io/properties')
      .then(response => response.data.data)
      .then(data => this.setState({data, dataIsLoading:false}))
      .catch(error => this.setState({error, dataIsLoading: false}))

      axios.get('http://demo4452328.mockable.io/templates')
      .then(response => response.data)
      .then(template => this.setState({template, templateIsLoading: false}))
      .catch(error => this.setState({error, templateIsLoading: false}))
  }

  render() {
    let {data, template, displayOption, dataIsLoading, templateIsLoading} = this.state;
    return (
        <div>
            {(dataIsLoading || templateIsLoading) && <h1>LOADING...</h1>}
            <ul className={style.list}>
            {data.map(x => <Item imageSrc={x.images[0]} address={x.full_address} price={x.price} area={x.area} template={template} displayOption={displayOption}/>)}
            <Button onClick={this.handleOnClic} text='1'/>
            </ul>
        </div>
    );
  }
  }

export default App;
