import React from 'react';
import style from './Address.css';

const Address = ({address}) => {
    return (
        <p className={style.address}>{address}</p>
    );
};

export default Address;