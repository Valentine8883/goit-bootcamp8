import React from 'react';
import style from './Card.css';
import Image from '../Image/image.jsx';
import Address from '../Address/Address.jsx';
import Price from '../Price/Price.jsx';
import Area from '../Area/Area.jsx';


const Card = ({imageSrc, price, address, area, template, displayOption}) => {
    return (
        <li className={style.item}>
            {template[0].template.map(x =>  x.component === 'IMAGE' ?  
            <Image image={imageSrc}/> : x.component === 'ADDRESS' ? 
            <Address address={address}/> : x.component === "PRICE"? 
            <Price price={price}/> : x.component === "AREA" ? 
            <Area area={area}/> : null)}
        </li>
    );
};

export default Card;


// const Card = ({template, address, imageSrc, area, price}) => {
//     const paint = (str, el, status) => {
//         return str === "IMAGE" ? <Image image={image} pr={el.hasOwnProperty('children') ? paint(el.children[0].component, el, true)  : null} status={status}/> : str === "ADDRESS" ? <Addres full_address={full_address} status={status}/> : str === "PRICE" ? <Price price={price} status={status}/> : str === "AREA" ? <Area area={area} status={status}/> : null;
//     }
//         return (
//         <div className={styles.card}>
//             {templates[2].template.map(el => paint(el.component, el , false))}
//         </div>
//     );
// };